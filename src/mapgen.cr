require "./bearlibmg"

module BearMG
  def self.generate_map(size : Int32, typ : BearLibMG::MapGenerator, seed : Int32) : Array(BearLibMG::CellType)
    raw_data = Array(BearLibMG::CellType).new(size*size, BearLibMG::CellType.new(0))
    tuple = {raw_data, size}
    box = Box.box(tuple)
    BearLibMG.generate_cb(size, size, typ, seed,
      ->(x, y, cell, opaque) do
        atuple = Box(typeof(tuple)).unbox(opaque)
        atuple[0][atuple[1]*y + x] = cell
      end,
      box, nil, nil)
    raw_data
  end
end

enum BearLibMG::CellType
  def passable?
    {BearLibMG::CellType::TILE_GROUND,
     BearLibMG::CellType::TILE_WATER,
     BearLibMG::CellType::TILE_ROAD,
     BearLibMG::CellType::TILE_HOUSE_FLOOR,
     BearLibMG::CellType::TILE_GRASS,

     BearLibMG::CellType::TILE_CORRIDOR,
     BearLibMG::CellType::TILE_SMALLROOM,
     BearLibMG::CellType::TILE_BIGROOM,
     BearLibMG::CellType::TILE_DOOR,
    }.includes? self
  end
end
