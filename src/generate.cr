class Processor
  def fine_name(s)
    return s if s == "RES"
    s = s.chars.select { |c| c.ascii_alphanumeric? || c == '_' }.join
    s = "id_" + s if s == "" || s[0].ascii_number?
    s.capitalize
  end

  USED_NAMES = Set(String).new
  RES_IDS    = Hash(String, Int32).new(0)
  @indent = 0
  @need_space = false

  def unique_name(s)
    if USED_NAMES.includes? s
      n = 1
      while USED_NAMES.includes? s + "_" + n.to_s
        n += 1
      end
      s = s + "_" + n.to_s
    end
    USED_NAMES << s
    s
  end

  GRAPH_EXTS = ["bmp", "dds", "jpg", "png", "tga", "psd"]
  SOUND_EXTS = ["wav", "ogg", "flac"]
  FONT_EXTS  = ["ttf", "otf", "fnt"]

  def print_res(res, item)
    item = unique_name(fine_name(item))
    print "#{item} = #{res}.new(#{RES_IDS[res]})"
    RES_IDS[res] = RES_IDS[res] + 1
  end

  def detect_dir(adir, andprint)
    if m = /(.*)_font$/.match(adir)
      print_res "Font", m[1] if andprint
      return true
    end
    if m = /(.*)_button$/.match(adir)
      print_res "ButtonResource", m[1] if andprint
      return true
    end
    false
  end

  def detect_file(afile)
    m = /(.*)\.([^\.]*)$/.match(afile)
    raise afile unless m
    name, ext = m[1], m[2]
    if GRAPH_EXTS.includes? ext
      if m = /(.*)_tiled_([[:digit:]]*)x([[:digit:]]*)$/.match(name)
        print_res "TileMap", m[1]
        return
      end

      print_res "Sprite", name
      return
    end
    if SOUND_EXTS.includes? ext
      print_res "Sound", name
      return
    end
    if FONT_EXTS.includes? ext
      print_res "FontResource", name
      return
    end
    print_res "RawResource", name
  end

  def print(s)
    if @need_space
      @out_file.puts ""
      @need_space = false
    end
    @out_file.puts "#{" "*@indent}#{s}"
  end

  def process_dir(short, adir)
    print "module #{fine_name(short)}"
    @indent += 2
    Dir.each(adir) do |item|
      next if item == "." || item == ".."
      process_dir item, adir + "/" + item if Dir.exists?(adir + "/" + item) && !detect_dir(item, false)
    end
    Dir.each(adir) do |item|
      next if item == "." || item == ".."
      if Dir.exists?(adir + "/" + item)
        detect_dir(item, true)
      else
        detect_file(item)
      end
    end
    @indent -= 2
    @need_space = false
    print "end"
    @need_space = true
  end

  @path : String
  @out_file : File

  def initialize(@path, out_filename)
    @out_file = File.open(out_filename, "w")
  end

  def go
    print "include Engine

THE_SCREEN = Sprite.new(-1)
NO_MUSIC = Sound.new(-1)
    "

    process_dir "RES", @path
    @out_file.close
  end
end

Processor.new(ARGV.size > 0 ? ARGV[0] : "./resources", "./src/resources.cr").go
