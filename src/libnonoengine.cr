@[Link("nonoengine")]
lib LibEngine
  alias Coord = Float32
  alias Color = UInt32
  alias String = UInt8*
  type RawResource = Int32
  type Sprite = Int32
  type Sound = Int32
  type Button = Int32
  type FontInstance = Int32
  type TileMap = Int32
  type Font = Int32
  type Panel = Int32

  enum Key
    A; B; C; D; E; F; G; H
    I; J; K; L; M; N; O; P
    Q; R; S; T; U; V; W; X
    Y; Z
    Num0; Num1; Num2; Num3; Num4
    Num5; Num6; Num7; Num8; Num9

    Escape
    LControl; LShift; LAlt; LSystem
    RControl; RShift; RAlt; RSystem
    Menu
    LBracket  # [
    RBracket  # ]
    SemiColon # ;
    Comma     # ,
    Period    # .
    Quote     # '
    Slash     # /
    BackSlash # \
    Tilde     # ~
    Equal     # =
    Dash      # -
    Space
    Return
    Backspace
    Tab
    PageUp
    PageDown
    End
    Home
    Insert
    Delete
    Add      # +
    Subtract # -
    Multiply # *
    Divide   # /
    Left     # Left arrow
    Right    # Right arrow
    Up       # Up arrow
    Down     # Down arrow
    Numpad0; Numpad1; Numpad2; Numpad3; Numpad4
    Numpad5; Numpad6; Numpad7; Numpad8; Numpad9
    F1; F2; F3; F4; F5; F6; F7; F8
    F9; F10; F11; F12; F13; F14; F15
    Pause

    # special keys

    Quit = -1
    Any  = -2
  end

  enum MouseButton
    Left
    Right
    Middle
  end

  enum MouseAxis
    X
    Y
    Scroll
  end

  enum KeyState
    Up
    Down
    Pressed
  end

  enum MouseButtonState
    Up
    Down
    Clicked
  end

  enum VAlign
    None
    Top
    Center
    Bottom
    Flow
  end
  enum HAlign
    None
    Left
    Center
    Right
    Flow
  end

  enum EngineValue
    Fullscreen
    Width
    Height
    VSync
    Antialias
    UseLog
    Autoscale
    Volume
    ClearColor
    RealWidth
    RealHeight
    FPS
    DeltaTime
  end
  # TEngineConfig = Fullscreen..ClearColor;

  @[Flags]
  enum FontStyle
    Bold
    Italic
    Underlined
  end

  enum ButtonState
    Normal
    Hover
    Pressed
    Clicked
  end

  enum GUICoord
    X
    Y
    Width
    Height
    MouseX
    MouseY
  end

  enum PathfindAlgorithm
    AStarNew
    AStarReuse
    DijkstraNew
    DijkstraReuse
  end
  type PathfindCallback = (Float32, Float32, Float32, Float32, Void* -> Float32)

  # Общие функции движка

  fun init = EngineInit(resources : String)
  fun set = EngineSet(param : EngineValue, value : Int32)
  fun get = EngineGet(param : EngineValue) : Int32
  fun process = EngineProcess
  fun raw_resource = RawResource(resource : RawResource) : Void*
  fun raw_texture = RawTexture(resource : Sprite) : UInt32 # returns opengl handle
  fun log = EngineLog(s : String)

  # Обработка ввода

  fun key_state = KeyState(key : Key) : KeyState
  fun mouse_get = MouseGet(axis : MouseAxis) : Coord
  fun mouse_state = MouseState(key : MouseButton) : MouseButtonState

  # 2д-рендер - спрайты

  fun sprite = Sprite(sprite : Sprite, x : Coord, y : Coord, kx : Float32, ky : Float32, angle : Float32, color : Color)
  fun draw_tiled = DrawTiled(tiled : TileMap, frame : Int32, x : Coord, y : Coord, kx : Float32, ky : Float32, angle : Float32, color : Color)
  fun background = Background(sprite : Sprite, kx : Float32, ky : Float32, dx : Float32, dy : Float32, color : Color)

  # 2д-рендер - примитивы

  fun line = Line(x1 : Coord, y1 : Coord, x2 : Coord, y2 : Coord, color1 : Color, color2 : Color)
  # procedure Line(x1, y1, x2, y2: TCoord; color: TColor); overload;
  fun line_settings = LineSettings(width : Float32, stipple : UInt32, stipple_scale : Float32)
  fun ellipse = Ellipse(x : Coord, y : Coord, rx : Coord, ry : Coord, filled : Bool, color1 : Color, color2 : Color, angle : Float32)
  fun rect = Rect(x0 : Coord, y0 : Coord, w : Coord, h : Coord, filled : Bool, color1 : Color, color2 : Color, color3 : Color, color4 : Color, angle : Float32)
  fun point = Point(x : Coord, y : Coord, color : Color)
  fun triangle = Triangle(x1 : Coord, y1 : Coord, color1 : Color, x2 : Coord, y2 : Coord, color2 : Color, x3 : Coord, y3 : Coord, color3 : Color)

  fun textured_triangle = TexturedTriangle(sprite : Sprite, x1 : Coord, y1 : Coord, tx1 : Coord, ty1 : Coord, x2 : Coord, y2 : Coord, tx2 : Coord, ty2 : Coord, x3 : Coord, y3 : Coord, tx3 : Coord, ty3 : Coord)

  # 2д-рендер - дополнительные функции

  fun get_pixel = GetPixel(x : Coord, y : Coord, sprite : Sprite) : Color
  fun layer = SetLayer(z : Int32)
  fun camera = Camera(dx : Coord, dy : Coord, kx : Float32, ky : Float32, angle : Float32)
  fun render_to = RenderTo(sprite : Sprite)
  fun set_pixel = SetPixel(sprite : Sprite, x : Coord, y : Coord, color : Color)

  # 2д-рендер - вывод текста

  fun font_config = FontConfig(font : FontInstance, char_size : Int32, color : Color, styles : FontStyle, kx : Float32, ky : Float32)
  fun font_create = FontCreate(font : Font, char_size : Int32, color : Color, styles : FontStyle, kx : Float32, ky : Float32) : FontInstance
  fun draw_text = DrawText(font : FontInstance, text : String, x : Coord, y : Coord)
  fun draw_text_boxed = DrawTextBoxed(font : FontInstance, text : String, x : Coord, y : Coord, w : Coord, h : Coord, halign : HAlign, valign : VAlign)

  # ГУИ

  fun panel = Panel(id : Panel, parent : Panel, x : Coord, y : Coord, w : Coord, h : Coord, halign : HAlign, valign : VAlign)
  fun button = Button(btn : Button, parent : Panel, x : Coord, y : Coord, w : Coord, h : Coord, halign : HAlign, valign : VAlign, text : String, font : FontInstance, data : Void*) : ButtonState
  fun gui_coord = GetGUICoord(coord : GUICoord) : Coord

  # Звук

  fun play = Play(sound : Sound, volume : Float32, data : Void*)
  fun music = Music(sound : Sound, volume : Float32)
  fun sound_playing = SoundPlaying(sound : Sound, data : Void*) : Bool

  # Поиск пути
  fun pathfind = Pathfind(size_x : Int32, size_y : Int32, algorithm : PathfindAlgorithm, diagonal_cost : Float32,
                          fromx : Int32, fromy : Int32, tox : Int32, toy : Int32,
                          x : Int32*, y : Int32*,
                          callback : PathfindCallback, opaque : Void*)
end

module Engine
  enum Params
    Fullscreen
    Width
    Height
    VSync
    Antialias
    UseLog
    Autoscale
    Volume
    ClearColor
    RealWidth
    RealHeight
    FPS
    DeltaTime
  end

  def self.[]=(param : Params, value)
    LibEngine.set(LibEngine::EngineValue.new(param.to_i), value)
  end

  def self.[](param : Params)
    LibEngine.get(LibEngine::EngineValue.new(param.to_i))
  end

  record PanelMetrics, pos : {Float32, Float32}, size : {Float32, Float32}, cursor : {Float32, Float32} do
    def self.read_from_engine
      PanelMetrics.new(
        pos: {LibEngine.gui_coord(LibEngine::GUICoord::X), LibEngine.gui_coord(LibEngine::GUICoord::Y)},
        size: {LibEngine.gui_coord(LibEngine::GUICoord::Width), LibEngine.gui_coord(LibEngine::GUICoord::Height)},
        cursor: {LibEngine.gui_coord(LibEngine::GUICoord::MouseX), LibEngine.gui_coord(LibEngine::GUICoord::MouseY)}
      )
    end

    def center
      {pos[0] + size[0]/2, pos[1] + size[1]/2}
    end
  end

  class GUI
    @@panel_counter = 0
    @@panel_parent = 0
    @@button_counter = 0
    # TODO - screen size
    @@metrics = PanelMetrics.new({0f32, 0f32}, {0f32, 0f32}, {0f32, 0f32})

    def self.reset
      @@panel_counter = 0
      @@panel_parent = 0
      @@button_counter = 0
      @@metrics = PanelMetrics.new({0f32, 0f32}, {0f32, 0f32}, {0f32, 0f32})
    end

    def self.parent
      @@panel_parent.unsafe_as(LibEngine::Panel)
    end

    def self.metrics
      @@metrics
    end

    def self.metrics=(x)
      @@metrics = x
    end

    def self.read_metrics
      @@metrics = PanelMetrics.read_from_engine
    end

    def self.parent=(x)
      @@panel_parent = x.to_i
    end

    def self.new_panel
      @@panel_counter += 1
      @@panel_counter.unsafe_as(LibEngine::Panel)
    end

    def self.new_button
      @@button_counter += 1
      @@button_counter.unsafe_as(Pointer(Void))
    end

    def self.pos
      @@metrics.pos
    end

    def self.size
      @@metrics.size
    end

    def self.center
      @@metrics.center
    end

    def self.cursor
      @@metrics.cursor
    end

    def self.button_state
      xg, yg = GUI.cursor
      inside = (0..1).includes?(xg) && (0..1).includes?(yg)
      state = ButtonState::Normal
      if inside
        if Mouse.left.clicked?
          state = ButtonState::Clicked
        elsif Mouse.left.down?
          state = ButtonState::Pressed
        else
          state = ButtonState::Hover
        end
      end
      state
    end
  end

  def process
    LibEngine.process
    GUI.reset
  end

  def log(s)
    LibEngine.log(s.to_unsafe)
  end

  struct Sprite
    @data : Int32

    def initialize(@data)
    end

    def to_unsafe
      @data.unsafe_as(LibEngine::Sprite)
    end

    def draw(x, y, kx = 1.0, ky = 1.0, angle = 0.0, color = Color::WHITE)
      LibEngine.sprite(to_unsafe, x, y, kx, ky, angle, color)
    end

    def background(kx = 1.0, ky = 1.0, dx = 0.0, dy = 0.0, color = Color::WHITE)
      LibEngine.background(to_unsafe, kx, ky, dx, dy, color)
    end

    def tex_triangle(x1, y1, tx1, ty1, x2, y2, tx2, ty2, x3, y3, tx3, ty3)
      LibEngine.textured_triangle(to_unsafe, x1, y1, tx1, ty1, x2, y2, tx2, ty2, x3, y3, tx3, ty3)
    end

    def tex_triangle(x1, y1, x2, y2, x3, y3, dx = 0, dy = 0, kx = 1, ky = 1)
      LibEngine.textured_triangle(to_unsafe,
        x1, y1, dx, dy,
        x2, y2, dx + (x2 - x1) * kx, dy + (y2 - y1) * ky,
        x3, y3, dx + (x3 - x1) * kx, dy + (y3 - y1) * ky)
    end
  end

  struct TileMap
    @data : Int32

    def initialize(@data)
    end

    def to_unsafe
      @data.unsafe_as(LibEngine::TileMap)
    end

    def draw_frame(frame, x, y, kx = 1.0, ky = 1.0, angle = 0.0, color = Color::WHITE)
      LibEngine.draw_tiled(to_unsafe, frame, x, y, kx, ky, angle, color)
    end
  end

  class Sound
    @data : Int32

    def initialize(@data)
    end

    def to_unsafe
      @data.unsafe_as(LibEngine::Sound)
    end

    def clone
      Sound.new(@data)
    end

    def play(volume = 100.0, data = nil)
      LibEngine.play(to_unsafe, volume, self.as(Void*))
    end

    def music(volume = 100.0)
      LibEngine.music(to_unsafe, volume)
    end

    def playing?(data = nil)
      LibEngine.sound_playing(to_unsafe, self.as(Void*))
    end
  end

  enum VAlign
    None
    Top
    Center
    Bottom
    Flow
  end
  enum HAlign
    None
    Left
    Center
    Right
    Flow
  end

  struct FontResource
    @data : Int32

    def initialize(@data)
    end

    def to_unsafe
      @data.unsafe_as(LibEngine::Font)
    end
  end

  struct Font
    BOLD       = 1
    ITALIC     = 2
    UNDERLINED = 4

    @data : LibEngine::FontInstance

    def to_unsafe
      @data.unsafe_as(LibEngine::FontInstance)
    end

    DEFAULT_CONFIG = {char_size: 24, color: Engine::Color::WHITE, styles: 0, kx: 1, ky: 1}

    def initialize(res : FontResource, *, char_size = DEFAULT_CONFIG[:char_size], color = DEFAULT_CONFIG[:color], styles = DEFAULT_CONFIG[:styles], kx = DEFAULT_CONFIG[:kx], ky = DEFAULT_CONFIG[:ky])
      @data = LibEngine.font_create(res.to_unsafe, char_size, color, LibEngine::FontStyle.new(styles), kx, ky)
    end

    def self.new_raw(data)
      new(FontResource.new(data))
    end

    def draw_text(text, x, y)
      LibEngine.draw_text(to_unsafe, text, x, y)
    end

    def draw_text_boxed(text, x, y, w, h, halign : HAlign = HAlign::Left, valign : VAlign = VAlign::Center)
      LibEngine.draw_text_boxed(to_unsafe, text, x, y, w, h, LibEngine::HAlign.new(halign.to_i), LibEngine::VAlign.new(valign.to_i))
    end
  end

  LAYER_GUI = 100

  def self.layer=(value)
    LibEngine.layer(value)
  end

  def camera(dx = 0, dy = 0, kx = 1, ky = 1, angle = 0)
    LibEngine.camera(dx, dy, kx, ky, angle)
  end

  enum Color : UInt32
    BLACK      = 0x000000FF
    MAROON     = 0x800000FF
    GREEN      = 0x008000FF
    OLIVE      = 0x808000FF
    NAVY       = 0x000080FF
    PURPLE     = 0x800080FF
    TEAL       = 0x008080FF
    GRAY       = 0x808080FF
    SILVER     = 0xC0C0C0FF
    RED        = 0xFF0000FF
    LIME       = 0x00FF00FF
    YELLOW     = 0xFFFF00FF
    BLUE       = 0x0000FFFF
    FUCHSIA    = 0xFF00FFFF
    AQUA       = 0x00FFFFFF
    WHITE      = 0xFFFFFFFF
    MONEYGREEN = 0xC0DCC0FF
    SKYBLUE    = 0xA6CAF0FF
    CREAM      = 0xFFFBF0FF
    MEDGRAY    = 0xA0A0A4FF
  end

  def color(r : Int32, g : Int32, b : Int32, a : Int32 = 255) : Engine::Color
    Color.new((UInt32.new(r) << 24) + (UInt32.new(g) << 16) + (UInt32.new(b) << 8) + (UInt32.new(a) << 0))
  end

  def color(u : UInt32) : Engine::Color
    Color.new(u)
  end

  def line(x1, y1, x2, y2, color1, color2 = color1)
    LibEngine.line(x1, y1, x2, y2, color1, color2)
  end

  def line_settings(width = 1, stipple = 0xFFFFFFFF, stipple_scale = 1)
    LibEngine.line_settings(width, stipple, stipple_scale)
  end

  def ellipse(x, y, rx, ry, filled, color1, color2 = color1, angle = 0)
    LibEngine.ellipse(x, y, rx, ry, filled, color1, color2, angle)
  end

  def circle(x, y, r, filled, color1, color2 = color1)
    LibEngine.ellipse(x, y, r, r, filled, color1, color2)
  end

  def rect(x0, y0, w, h, filled, color1, color2, color3, color4, angle = 0)
    LibEngine.rect x0, y0, w, h, filled, color1, color2, color3, color4, angle
  end

  def rect(x0, y0, w, h, filled, color, angle = 0)
    LibEngine.rect x0, y0, w, h, filled, color, color, color, color, angle
  end

  def rect_gauge(x0, y0, w, h, value, color1, color2, angle = 0)
    LibEngine.rect x0, y0, w, h, true, color2, color2, color2, color2, angle
    LibEngine.rect x0, y0, value*w, h, true, color1, color1, color1, color1, angle
  end

  def point(x, y, color)
    LibEngine.point(x, y, color)
  end

  def triangle(x1, y1, color1, x2, y2, color2, x3, y3, color3)
    LibEngine.triangle x1, y1, color1, x2, y2, color2, x3, y3, color3
  end

  def triangle(x1, y1, x2, y2, x3, y3, color)
    LibEngine.triangle x1, y1, color, x2, y2, color, x3, y3, color
  end

  def panel(x = 0, y = 0, width = 0, height = 0, halign = HAlign::None, valign = VAlign::None, fill : Color? = nil, border : Color? = nil, &)
    own_id = GUI.new_panel
    parent = GUI.parent
    metrics = GUI.metrics
    LibEngine.panel(own_id, parent, x, y, width, height, LibEngine::HAlign.new(halign.to_i), LibEngine::VAlign.new(valign.to_i))
    GUI.read_metrics

    rect(*GUI.pos, *GUI.size, true, fill) if fill
    rect(*GUI.pos, *GUI.size, false, border) if border
    GUI.parent = own_id
    x = yield
    GUI.parent = parent
    GUI.metrics = metrics
    x
  end

  def panel(*args, **args2)
    panel(*args, **args2) do
    end
  end

  def label(txt : String, font : Font, x = 0, y = 0, width = 0, height = 0, halign = HAlign::None, valign = VAlign::None, fill : Color? = nil, border : Color? = nil, text_halign = HAlign::None, text_valign = VAlign::None)
    panel(x, y, width, height, halign, valign, fill, border) do
      font.draw_text_boxed(txt, *GUI.pos, *GUI.size, halign: text_halign, valign: text_valign)
    end
  end

  enum ButtonState
    Normal
    Hover
    Pressed
    Clicked
  end

  def button(resource : ButtonResource, x = 0, y = 0, width = 0, height = 0, text : String? = nil, halign : HAlign = HAlign::None, valign : VAlign = VAlign::None, font : Font? = nil)
    # own_id = GUI.new_button
    LibEngine.button(resource.to_unsafe, GUI.parent, x, y, width, height, LibEngine::HAlign.new(halign.to_i), LibEngine::VAlign.new(valign.to_i), (text ? text.to_unsafe : 0.unsafe_as(Pointer(UInt8))), font || Font.new_raw(0), nil).unsafe_as(ButtonState)
  end

  def button(*args, **args2, &)
    metrics = GUI.metrics
    state = button(*args, **args2)
    GUI.read_metrics
    x = yield(state) unless state.normal?
    GUI.metrics = metrics
    x
  end

  def button_clicked(*args, **args2, &)
    metrics = GUI.metrics
    state = button(*args, **args2)
    GUI.read_metrics
    x = yield if state.clicked?
    GUI.metrics = metrics
    x
  end

  struct ButtonResource
    @data : Int32

    def initialize(@data)
    end

    def to_unsafe
      @data.unsafe_as(LibEngine::Button)
    end
  end

  alias Key = LibEngine::Key

  module Keys
    def self.[](x)
      LibEngine.key_state(x)
    end
  end

  enum MouseButtonState
    Up
    Down
    Clicked
  end

  module Mouse
    def self.x
      LibEngine.mouse_get(LibEngine::MouseAxis::X)
    end

    def self.y
      LibEngine.mouse_get(LibEngine::MouseAxis::Y)
    end

    def self.scroll
      LibEngine.mouse_get(LibEngine::MouseAxis::Scroll)
    end

    def self.left
      MouseButtonState.new(LibEngine.mouse_state(LibEngine::MouseButton::Left).to_i)
    end

    def self.right
      MouseButtonState.new(LibEngine.mouse_state(LibEngine::MouseButton::Right).to_i)
    end

    def self.middle
      MouseButtonState.new(LibEngine.mouse_state(LibEngine::MouseButton::Middle).to_i)
    end
  end
end
