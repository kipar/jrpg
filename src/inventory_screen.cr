require "./libnonoengine.cr"
require "./scales"
require "./map"

class InventoryWindow
  getter! game : Game
  getter! player : Player
  property visible = false
  property for_battle : Battle?

  @u_tooltip : SomeUnit? = nil

  def initialize(@game)
    @player = game.player
  end

  def show
    @visible = true
    @for_battle = nil
    sort_inventory
  end

  def show_for(b)
    @visible = true
    @for_battle = b
    sort_inventory
  end

  def draw
    @u_tooltip = nil
    Engine.layer = LAYER_GUI + 1
    panel(100, 50, 400, 600, fill: color(0x505060ff)) do
      label("Мои зверушки", FONTS[UseFont::Usual], 0, -20, 380, 40, HAlign::Center, VAlign::Top, fill: color(0x000000ff), text_halign: HAlign::Center, text_valign: VAlign::Center)
      basex, basey = GUI.pos
      sx, sy = GUI.size
      basex -= 64 + 32 # TODO omg crunch
      curx, cury = basex, basey
      dx = 64 + 2
      dy = 64 + 2
      width = sx
      player.inventory.each do |u|
        next if @for_battle && (@for_battle.not_nil!.hp_for_reserve[u]? || 1) <= 0
        panel(curx, cury, 64, 64) do
          @u_tooltip = u unless GUI.button_state.normal?
          if GUI.button_state.clicked?
            if battle = @for_battle
              if (battle.hp_for_reserve[u]? || 1) > 0
                battle.inv_selected(u)
                @for_battle = nil
                @visible = false
                return
              end
            else
              move_to_equip(u)
            end
          end
          show_monster_card(u.tile, u.level)
          curx += dx
          if curx >= width
            cury += dy
            curx = basex
          end

          if unit_hp(u) < u.params[UPS::MaxHP]
            panel(0, 15, 54, 10, HAlign::Center, VAlign::Bottom, fill: color(0xff0000ff)) do
              rect_gauge(*GUI.pos, *GUI.size, unit_hp(u) / u.params[UPS::MaxHP], color(0x00ff00ff), color(0xff0000ff))
            end
          end
        end
      end
    end

    if !@for_battle
      panel(300, -100, 100, 400, HAlign::Right, VAlign::Center, fill: color(0x006060ff)) do
        label("Авангард", FONTS[UseFont::Usual], 0, -50, 180, 40, HAlign::Center, VAlign::Top, fill: color(0x000000ff), text_halign: HAlign::Center, text_valign: VAlign::Center)
        # lets show equip monsters
        player.equip.each_with_index do |u, i|
          panel(0, 50, 64, 64, halign: HAlign::Center, valign: VAlign::Flow) do
            st = GUI.button_state
            if u
              show_monster_card(u.tile, u.level)
              @u_tooltip = u unless st.normal?
              if st.clicked?
                move_to_inventory(i)
              end
            else
              rect(*GUI.pos, *GUI.size, false, 0x101010ff)
            end
          end
        end
      end

      FONTS[UseFont::Log].draw_text_boxed("Секретный счетчик: #{game.killed_top_level} из 5", 0, 0, 1024, 768, halign: HAlign::Right, valign: VAlign::Bottom)
    end

    show_tooltip
  end

  def move_to_equip(u)
    i = player.equip.index(&.nil?)
    if i.nil?
      move_to_inventory(0)
      i = 0
    end
    player.inventory.delete(u)
    player.equip[i] = u
  end

  def move_to_inventory(i)
    player.inventory << player.equip[i].not_nil!
    sort_inventory
    player.equip[i] = nil
  end

  def sort_inventory
    player.inventory.sort_by! { |u| {-u.level, u.typ} }
  end

  def process
    if Keys[Key::Escape].pressed?
      @visible = false
      return
    end
  end

  def multiline(font, x0, y0, dy, list)
    ypos = 0
    list.each do |str|
      font.draw_text(str, x0, y0 + ypos)
      ypos += dy
    end
  end

  def unit_hp(unit)
    if b = @for_battle
      b.hp_for_reserve[unit]? || unit.params[UPS::MaxHP]
    else
      unit.params[UPS::MaxHP]
    end
  end

  def show_tooltip
    if u = @u_tooltip
      elems = Element.values.select { |elem| u.resistance[elem] != 0 }
      resistances = elems.map do |elem|
        "#{RESIST_NAMES[elem]}: #{u.resistance[elem]}"
      end

      ax = Mouse.x + 30
      ax = Mouse.x - 30 - 400 if ax + 400 > Engine[Params::Width]
      panel(ax, Mouse.y + 30, 400, 125 + resistances.size*12) do
        x0, y0 = GUI.pos

        rect(x0, y0, *GUI.size, true, 0x888888ff)
        rect(x0, y0, *GUI.size, false, grade_color(u.level))
        FONTS[UseFont::Small].draw_text(u.name + "(ур.#{u.level})", x0 + 5, y0)
        params = UPS.values.map do |param|
          case param
          when .max_hp?
            str = "#{unit_hp(u)}/#{u.params[param]}"
          when .max_sp?
            str = "#{u.params[param]}/#{u.params[param]}"
          else
            str = "#{u.params[param]}"
          end
          "#{PARAM_NAMES[param]}: #{str}"
        end
        attacks = u.attacks.map { |att|
          "#{att.sp > 0 ? "(#{att.sp}сп)" : ""}#{att.name}: #{att.power*u.params[UPS::Damage]//100} #{DAMAGE_NAMES[att.element]} урона"
        } + u.spells.map { |sp|
          "(#{sp.sp}сп)#{sp.name}"
        }

        multiline(FONTS[UseFont::Small], x0 + 10, y0 + 15, 12, params)
        multiline(FONTS[UseFont::Small], x0 + 110, y0 + 15, 12, attacks)
        FONTS[UseFont::Small].draw_text("Сопротивления:", x0 + 5, y0 + 100)
        multiline(FONTS[UseFont::Small], x0 + 10, y0 + 115, 12, resistances)
      end
    end
  end
end
