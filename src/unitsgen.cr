require "./battle"
require "./spells"

# def log(x)
#   puts x
# end

enum UTyp
  Beholder
  Swarm
  Wolf
  Crow
  Aspid
  Hydra
  Golem
  Grifon
  Bug
  Crane
  Rabbit
  Cobra
  Flee
  Cat
  Frog
  Bear
  Ant
  Fly
  Bat
  Bee
  Panther
  Spider
  Shade
  Skeleton
  Scorpion
  Slime
  Dog
  Hawk
  Tiger
  Anaconda
  Snail
  Phoenix
  Chimera
  Worm
  Bird
  Lizard
end

UNITS_DATA =
  {
    {level: 0, typ: UTyp::Bat, element: Element::None, tile: 89},
    {level: 0, typ: UTyp::Ant, element: Element::None, tile: 177},
    {level: 0, typ: UTyp::Rabbit, element: Element::None, tile: 357},
    {level: 0, typ: UTyp::Snail, element: Element::None, tile: 358},
    {level: 0, typ: UTyp::Worm, element: Element::None, tile: 195},
    {level: 0, typ: UTyp::Bug, element: Element::None, tile: 376},
    {level: 0, typ: UTyp::Crow, element: Element::None, tile: 387},
    {level: 0, typ: UTyp::Fly, element: Element::None, tile: 37},
    {level: 0, typ: UTyp::Fly, element: Element::None, tile: 39},
    {level: 0, typ: UTyp::Flee, element: Element::None, tile: 232},
    {level: 0, typ: UTyp::Beholder, element: Element::None, tile: 241},
    {level: 0, typ: UTyp::Beholder, element: Element::None, tile: 329},
    {level: 1, typ: UTyp::Slime, element: Element::Acid, tile: 0},
    {level: 1, typ: UTyp::Bat, element: Element::None, tile: 88},
    {level: 1, typ: UTyp::Ant, element: Element::Pierce, tile: 176},
    {level: 1, typ: UTyp::Golem, element: Element::None, tile: 264},
    {level: 1, typ: UTyp::Slime, element: Element::Cold, tile: 1},
    {level: 1, typ: UTyp::Golem, element: Element::None, tile: 265},
    {level: 1, typ: UTyp::Slime, element: Element::None, tile: 2},
    {level: 1, typ: UTyp::Bat, element: Element::None, tile: 90},
    {level: 1, typ: UTyp::Ant, element: Element::Cold, tile: 178},
    {level: 1, typ: UTyp::Ant, element: Element::Fire, tile: 179},
    {level: 1, typ: UTyp::Golem, element: Element::None, tile: 267},
    {level: 1, typ: UTyp::Beholder, element: Element::None, tile: 93},
    {level: 1, typ: UTyp::Crane, element: Element::None, tile: 270},
    {level: 1, typ: UTyp::Bat, element: Element::None, tile: 7},
    {level: 1, typ: UTyp::Bird, element: Element::None, tile: 271},
    {level: 1, typ: UTyp::Frog, element: Element::None, tile: 359},
    {level: 1, typ: UTyp::Hawk, element: Element::None, tile: 272},
    {level: 1, typ: UTyp::Frog, element: Element::None, tile: 360},
    {level: 1, typ: UTyp::Frog, element: Element::None, tile: 185},
    {level: 1, typ: UTyp::Frog, element: Element::None, tile: 361},
    {level: 1, typ: UTyp::Bat, element: Element::None, tile: 98},
    {level: 1, typ: UTyp::Bat, element: Element::Cold, tile: 99},
    {level: 1, typ: UTyp::Bat, element: Element::Poison, tile: 100},
    {level: 1, typ: UTyp::Bat, element: Element::None, tile: 188},
    {level: 1, typ: UTyp::Bat, element: Element::None, tile: 276},
    {level: 1, typ: UTyp::Lizard, element: Element::None, tile: 364},
    {level: 1, typ: UTyp::Bat, element: Element::None, tile: 189},
    {level: 1, typ: UTyp::Worm, element: Element::None, tile: 192},
    {level: 1, typ: UTyp::Worm, element: Element::Acid, tile: 193},
    {level: 1, typ: UTyp::Bug, element: Element::None, tile: 369},
    {level: 1, typ: UTyp::Shade, element: Element::None, tile: 457},
    {level: 1, typ: UTyp::Worm, element: Element::Electricity, tile: 194},
    {level: 1, typ: UTyp::Bug, element: Element::None, tile: 370},
    {level: 1, typ: UTyp::Bug, element: Element::Cold, tile: 371},
    {level: 1, typ: UTyp::Worm, element: Element::Mana, tile: 196},
    {level: 1, typ: UTyp::Swarm, element: Element::None, tile: 21},
    {level: 1, typ: UTyp::Aspid, element: Element::None, tile: 109},
    {level: 1, typ: UTyp::Worm, element: Element::Fire, tile: 197},
    {level: 1, typ: UTyp::Worm, element: Element::Chaos, tile: 285},
    {level: 1, typ: UTyp::Bug, element: Element::None, tile: 375},
    {level: 1, typ: UTyp::Aspid, element: Element::None, tile: 113},
    {level: 1, typ: UTyp::Aspid, element: Element::None, tile: 114},
    {level: 1, typ: UTyp::Aspid, element: Element::Acid, tile: 115},
    {level: 1, typ: UTyp::Skeleton, element: Element::None, tile: 475},
    {level: 1, typ: UTyp::Crow, element: Element::None, tile: 389},
    {level: 1, typ: UTyp::Fly, element: Element::None, tile: 38},
    {level: 1, typ: UTyp::Bee, element: Element::None, tile: 48},
    {level: 1, typ: UTyp::Cat, element: Element::None, tile: 138},
    {level: 1, typ: UTyp::Spider, element: Element::None, tile: 51},
    {level: 1, typ: UTyp::Spider, element: Element::None, tile: 52},
    {level: 1, typ: UTyp::Spider, element: Element::None, tile: 53},
    {level: 1, typ: UTyp::Spider, element: Element::None, tile: 54},
    {level: 1, typ: UTyp::Spider, element: Element::None, tile: 56},
    {level: 1, typ: UTyp::Flee, element: Element::Poison, tile: 233},
    {level: 1, typ: UTyp::Flee, element: Element::None, tile: 234},
    {level: 1, typ: UTyp::Flee, element: Element::Electricity, tile: 235},
    {level: 1, typ: UTyp::Flee, element: Element::None, tile: 236},
    {level: 1, typ: UTyp::Cat, element: Element::None, tile: 240},
    {level: 1, typ: UTyp::Dog, element: Element::None, tile: 153},
    {level: 1, typ: UTyp::Scorpion, element: Element::Fire, tile: 336},
    {level: 1, typ: UTyp::Scorpion, element: Element::Electricity, tile: 337},
    {level: 1, typ: UTyp::Scorpion, element: Element::Cold, tile: 338},
    {level: 1, typ: UTyp::Dog, element: Element::None, tile: 78},
    {level: 2, typ: UTyp::Slime, element: Element::None, tile: 352},
    {level: 2, typ: UTyp::Shade, element: Element::None, tile: 440},
    {level: 2, typ: UTyp::Slime, element: Element::Chaos, tile: 353},
    {level: 2, typ: UTyp::Shade, element: Element::Mana, tile: 441},
    {level: 2, typ: UTyp::Golem, element: Element::None, tile: 266},
    {level: 2, typ: UTyp::Slime, element: Element::None, tile: 354},
    {level: 2, typ: UTyp::Shade, element: Element::None, tile: 442},
    {level: 2, typ: UTyp::Slime, element: Element::None, tile: 3},
    {level: 2, typ: UTyp::Bat, element: Element::Cold, tile: 91},
    {level: 2, typ: UTyp::Slime, element: Element::Fire, tile: 355},
    {level: 2, typ: UTyp::Bat, element: Element::Fire, tile: 92},
    {level: 2, typ: UTyp::Ant, element: Element::None, tile: 180},
    {level: 2, typ: UTyp::Golem, element: Element::None, tile: 268},
    {level: 2, typ: UTyp::Slime, element: Element::Pierce, tile: 356},
    {level: 2, typ: UTyp::Ant, element: Element::Cold, tile: 181},
    {level: 2, typ: UTyp::Beholder, element: Element::Mana, tile: 94},
    {level: 2, typ: UTyp::Ant, element: Element::Poison, tile: 182},
    {level: 2, typ: UTyp::Beholder, element: Element::Acid, tile: 95},
    {level: 2, typ: UTyp::Ant, element: Element::Fire, tile: 183},
    {level: 2, typ: UTyp::Ant, element: Element::None, tile: 535},
    {level: 2, typ: UTyp::Beholder, element: Element::Poison, tile: 96},
    {level: 2, typ: UTyp::Bear, element: Element::Cold, tile: 184},
    {level: 2, typ: UTyp::Shade, element: Element::None, tile: 448},
    {level: 2, typ: UTyp::Beholder, element: Element::None, tile: 97},
    {level: 2, typ: UTyp::Frog, element: Element::Poison, tile: 273},
    {level: 2, typ: UTyp::Bug, element: Element::None, tile: 277},
    {level: 2, typ: UTyp::Lizard, element: Element::None, tile: 365},
    {level: 2, typ: UTyp::Grifon, element: Element::None, tile: 190},
    {level: 2, typ: UTyp::Bug, element: Element::Electricity, tile: 278},
    {level: 2, typ: UTyp::Lizard, element: Element::None, tile: 366},
    {level: 2, typ: UTyp::Bug, element: Element::Acid, tile: 279},
    {level: 2, typ: UTyp::Bug, element: Element::None, tile: 280},
    {level: 2, typ: UTyp::Anaconda, element: Element::None, tile: 105},
    {level: 2, typ: UTyp::Bug, element: Element::None, tile: 281},
    {level: 2, typ: UTyp::Anaconda, element: Element::None, tile: 106},
    {level: 2, typ: UTyp::Bug, element: Element::None, tile: 282},
    {level: 2, typ: UTyp::Anaconda, element: Element::Cold, tile: 107},
    {level: 2, typ: UTyp::Bug, element: Element::Cold, tile: 283},
    {level: 2, typ: UTyp::Anaconda, element: Element::None, tile: 108},
    {level: 2, typ: UTyp::Bug, element: Element::Fire, tile: 284},
    {level: 2, typ: UTyp::Bug, element: Element::Poison, tile: 372},
    {level: 2, typ: UTyp::Swarm, element: Element::None, tile: 22},
    {level: 2, typ: UTyp::Aspid, element: Element::None, tile: 110},
    {level: 2, typ: UTyp::Worm, element: Element::None, tile: 198},
    {level: 2, typ: UTyp::Worm, element: Element::Chaos, tile: 286},
    {level: 2, typ: UTyp::Swarm, element: Element::None, tile: 23},
    {level: 2, typ: UTyp::Aspid, element: Element::None, tile: 111},
    {level: 2, typ: UTyp::Swarm, element: Element::Fire, tile: 24},
    {level: 2, typ: UTyp::Cobra, element: Element::None, tile: 112},
    {level: 2, typ: UTyp::Skeleton, element: Element::None, tile: 552},
    {level: 2, typ: UTyp::Bug, element: Element::None, tile: 377},
    {level: 2, typ: UTyp::Bug, element: Element::None, tile: 378},
    {level: 2, typ: UTyp::Chimera, element: Element::None, tile: 381},
    {level: 2, typ: UTyp::Bear, element: Element::None, tile: 386},
    {level: 2, typ: UTyp::Worm, element: Element::None, tile: 474},
    {level: 2, typ: UTyp::Crow, element: Element::None, tile: 388},
    {level: 2, typ: UTyp::Spider, element: Element::None, tile: 133},
    {level: 2, typ: UTyp::Fly, element: Element::None, tile: 46},
    {level: 2, typ: UTyp::Spider, element: Element::Poison, tile: 134},
    {level: 2, typ: UTyp::Fly, element: Element::Fire, tile: 47},
    {level: 2, typ: UTyp::Fly, element: Element::None, tile: 49},
    {level: 2, typ: UTyp::Tiger, element: Element::None, tile: 140},
    {level: 2, typ: UTyp::Panther, element: Element::None, tile: 141},
    {level: 2, typ: UTyp::Cat, element: Element::None, tile: 142},
    {level: 2, typ: UTyp::Dog, element: Element::None, tile: 146},
    {level: 2, typ: UTyp::Dog, element: Element::None, tile: 147},
    {level: 2, typ: UTyp::Dog, element: Element::None, tile: 148},
    {level: 2, typ: UTyp::Dog, element: Element::Fire, tile: 149},
    {level: 2, typ: UTyp::Flee, element: Element::None, tile: 237},
    {level: 2, typ: UTyp::Dog, element: Element::Acid, tile: 150},
    {level: 2, typ: UTyp::Flee, element: Element::None, tile: 238},
    {level: 2, typ: UTyp::Dog, element: Element::Poison, tile: 151},
    {level: 2, typ: UTyp::Dog, element: Element::Electricity, tile: 152},
    {level: 2, typ: UTyp::Dog, element: Element::None, tile: 154},
    {level: 2, typ: UTyp::Dog, element: Element::None, tile: 155},
    {level: 2, typ: UTyp::Dog, element: Element::None, tile: 156},
    {level: 2, typ: UTyp::Dog, element: Element::None, tile: 157},
    {level: 2, typ: UTyp::Dog, element: Element::Mana, tile: 158},
    {level: 2, typ: UTyp::Scorpion, element: Element::None, tile: 334},
    {level: 2, typ: UTyp::Dog, element: Element::Cold, tile: 159},
    {level: 2, typ: UTyp::Scorpion, element: Element::None, tile: 335},
    {level: 2, typ: UTyp::Dog, element: Element::Chaos, tile: 160},
    {level: 2, typ: UTyp::Dog, element: Element::None, tile: 161},
    {level: 2, typ: UTyp::Dog, element: Element::Electricity, tile: 162},
    {level: 2, typ: UTyp::Dog, element: Element::None, tile: 75},
    {level: 2, typ: UTyp::Dog, element: Element::Electricity, tile: 163},
    {level: 2, typ: UTyp::Dog, element: Element::None, tile: 77},
    {level: 2, typ: UTyp::Dog, element: Element::Chaos, tile: 165},
    {level: 2, typ: UTyp::Wolf, element: Element::None, tile: 83},
    {level: 2, typ: UTyp::Wolf, element: Element::Cold, tile: 84},
    {level: 3, typ: UTyp::Bat, element: Element::Cold, tile: 529},
    {level: 3, typ: UTyp::Slime, element: Element::None, tile: 4},
    {level: 3, typ: UTyp::Golem, element: Element::Mana, tile: 269},
    {level: 3, typ: UTyp::Bat, element: Element::Fire, tile: 537},
    {level: 3, typ: UTyp::Ant, element: Element::Poison, tile: 186},
    {level: 3, typ: UTyp::Ant, element: Element::Poison, tile: 187},
    {level: 3, typ: UTyp::Bat, element: Element::None, tile: 191},
    {level: 3, typ: UTyp::Lizard, element: Element::None, tile: 367},
    {level: 3, typ: UTyp::Bug, element: Element::Fire, tile: 373},
    {level: 3, typ: UTyp::Bug, element: Element::None, tile: 374},
    {level: 3, typ: UTyp::Worm, element: Element::Chaos, tile: 199},
    {level: 3, typ: UTyp::Swarm, element: Element::Mana, tile: 25},
    {level: 3, typ: UTyp::Golem, element: Element::None, tile: 554},
    {level: 3, typ: UTyp::Worm, element: Element::None, tile: 204},
    {level: 3, typ: UTyp::Worm, element: Element::None, tile: 205},
    {level: 3, typ: UTyp::Worm, element: Element::None, tile: 206},
    {level: 3, typ: UTyp::Chimera, element: Element::None, tile: 382},
    {level: 3, typ: UTyp::Worm, element: Element::Fire, tile: 207},
    {level: 3, typ: UTyp::Worm, element: Element::None, tile: 208},
    {level: 3, typ: UTyp::Worm, element: Element::Cold, tile: 209},
    {level: 3, typ: UTyp::Golem, element: Element::None, tile: 297},
    {level: 3, typ: UTyp::Bear, element: Element::None, tile: 385},
    {level: 3, typ: UTyp::Worm, element: Element::Poison, tile: 210},
    {level: 3, typ: UTyp::Golem, element: Element::None, tile: 298},
    {level: 3, typ: UTyp::Shade, element: Element::None, tile: 562},
    {level: 3, typ: UTyp::Worm, element: Element::Electricity, tile: 211},
    {level: 3, typ: UTyp::Golem, element: Element::None, tile: 299},
    {level: 3, typ: UTyp::Worm, element: Element::Fire, tile: 212},
    {level: 3, typ: UTyp::Swarm, element: Element::Fire, tile: 300},
    {level: 3, typ: UTyp::Swarm, element: Element::Electricity, tile: 301},
    {level: 3, typ: UTyp::Worm, element: Element::None, tile: 214},
    {level: 3, typ: UTyp::Swarm, element: Element::Cold, tile: 302},
    {level: 3, typ: UTyp::Worm, element: Element::Chaos, tile: 215},
    {level: 3, typ: UTyp::Swarm, element: Element::Electricity, tile: 303},
    {level: 3, typ: UTyp::Swarm, element: Element::Poison, tile: 304},
    {level: 3, typ: UTyp::Swarm, element: Element::Mana, tile: 305},
    {level: 3, typ: UTyp::Swarm, element: Element::Chaos, tile: 308},
    {level: 3, typ: UTyp::Swarm, element: Element::None, tile: 310},
    {level: 3, typ: UTyp::Spider, element: Element::None, tile: 50},
    {level: 3, typ: UTyp::Tiger, element: Element::None, tile: 143},
    {level: 3, typ: UTyp::Cat, element: Element::None, tile: 145},
    {level: 3, typ: UTyp::Phoenix, element: Element::None, tile: 586},
    {level: 3, typ: UTyp::Wolf, element: Element::None, tile: 82},
    {level: 3, typ: UTyp::Dog, element: Element::Fire, tile: 87},
    {level: 4, typ: UTyp::Slime, element: Element::Acid, tile: 5},
    {level: 4, typ: UTyp::Lizard, element: Element::None, tile: 368},
    {level: 4, typ: UTyp::Worm, element: Element::Chaos, tile: 200},
    {level: 4, typ: UTyp::Hydra, element: Element::None, tile: 121},
    {level: 4, typ: UTyp::Hydra, element: Element::None, tile: 122},
    {level: 4, typ: UTyp::Hydra, element: Element::None, tile: 123},
    {level: 4, typ: UTyp::Shade, element: Element::None, tile: 40},
    {level: 4, typ: UTyp::Wolf, element: Element::None, tile: 85},
  }

UNITS_PREFIX = {
  {level: 2, name: "Ломающий", element: Element::Blunt},
  {level: 3, name: "Крушащий", element: Element::Blunt},
  {level: 2, name: "Разбивающий", element: Element::Blunt},
  {level: 3, name: "Громящий", element: Element::Blunt},
  {level: 1, name: "Колотящий", element: Element::Blunt},
  {level: 1, name: "Стучащий", element: Element::Blunt},

  {level: 1, name: "Режущий", element: Element::Slash},
  {level: 1, name: "Рубящий", element: Element::Slash},
  {level: 2, name: "Рассекающий", element: Element::Slash},
  {level: 1, name: "Секущий", element: Element::Slash},
  {level: 3, name: "Измельчающий", element: Element::Slash},

  {level: 1, name: "Колючий", element: Element::Pierce},
  {level: 1, name: "Острый", element: Element::Pierce},
  {level: 2, name: "Протыкающий", element: Element::Pierce},
  {level: 3, name: "Нанизывающий", element: Element::Pierce},
  {level: 2, name: "Пронзающий", element: Element::Pierce},
  {level: 2, name: "Пробивающий", element: Element::Pierce},

  {level: 1, name: "Огненный", element: Element::Fire},
  {level: 1, name: "Пылающий", element: Element::Fire},
  {level: 2, name: "Сжигающий", element: Element::Fire},
  {level: 2, name: "Испепеляющий", element: Element::Fire},
  {level: 4, name: "Адский", element: Element::Fire},
  {level: 3, name: "Магмовый", element: Element::Fire},
  {level: 3, name: "Лавовый", element: Element::Fire},

  {level: 2, name: "Кислотный", element: Element::Acid},
  {level: 3, name: "Растворяющий", element: Element::Acid},
  {level: 1, name: "Кислый", element: Element::Acid},

  {level: 1, name: "Холодный", element: Element::Cold},
  {level: 2, name: "Ледяной", element: Element::Cold},
  {level: 1, name: "Студеный", element: Element::Cold},
  {level: 3, name: "Замораживающий", element: Element::Cold},
  {level: 2, name: "Морозный", element: Element::Cold},
  {level: 1, name: "Искрящийся", element: Element::Electricity},
  {level: 2, name: "Электрический", element: Element::Electricity},
  {level: 3, name: "Громовой", element: Element::Electricity},
  {level: 3, name: "Плазменный", element: Element::Electricity},

  {level: 1, name: "Ядовитый", element: Element::Poison},
  {level: 2, name: "Отравляющий", element: Element::Poison},
  {level: 3, name: "Смертельный", element: Element::Poison},
  {level: 2, name: "Болезненный", element: Element::Poison},

  {level: 1, name: "Светящийся", element: Element::Light},
  {level: 2, name: "Блистающий", element: Element::Light},
  {level: 2, name: "Сверкающий", element: Element::Light},
  {level: 3, name: "Ослепительный", element: Element::Light},
  {level: 1, name: "Черный", element: Element::Dark},
  {level: 1, name: "Темный", element: Element::Dark},
  {level: 2, name: "Чудовищный", element: Element::Dark},
  {level: 3, name: "Беспросветный", element: Element::Dark},
  {level: 1, name: "Волшебный", element: Element::Mana},
  {level: 2, name: "Магический", element: Element::Mana},
  {level: 2, name: "Колдовской", element: Element::Mana},
  {level: 2, name: "Чародейский", element: Element::Mana},

  {level: 1, name: "Хаотичный", element: Element::Chaos},
  {level: 2, name: "Проклятый", element: Element::Chaos},
  {level: 3, name: "Неведомый", element: Element::Chaos},

}

COUNTER_ELEMENT = {
  Element::Fire  => Element::Cold,
  Element::Cold  => Element::Cold,
  Element::Light => Element::Dark,
  Element::Dark  => Element::Light,
  Element::Chaos => Element::Mana,
  Element::Mana  => Element::Chaos,
}

FREQUENT_ELEMENTS = {
  Element::Blunt,
  Element::Slash,
  Element::Pierce,
  Element::Poison,
}

SUFFIX_NAMES = {
  UPS::MaxHP      => {"-здоровяк", "-здоровяка"},
  UPS::MaxSP      => {"-марафонец", "-марафонца"},
  UPS::Damage     => {"-охотник", "-охотника"},
  UPS::Crit       => {"-убийца", "-убийцу"},
  UPS::Armor      => {"-броненосец", "-броненосца"},
  UPS::Initiative => {"-шустрик", "-шустрика"},
}

STAT_PER_POINT = {
  UPS::MaxHP      => 10,
  UPS::MaxSP      => 10,
  UPS::Damage     => 3,
  UPS::Crit       => 10,
  UPS::Armor      => 2,
  UPS::Initiative => 4,
}

BASIC_ATTACKS = {
  "когтит"         => Attack.new(Element::Slash, sp: 0, power: 100, name: "когтит"),
  "клюет"          => Attack.new(Element::Pierce, sp: 0, power: 120, name: "клюет"),
  "жалит"          => Attack.new(Element::Poison, sp: 5, power: 200, name: "жалит"),
  "кусает"         => Attack.new(Element::Pierce, sp: 1, power: 130, name: "кусает"),
  "рвет"           => Attack.new(Element::Slash, sp: 5, power: 200, name: "рвет"),
  "толкает"        => Attack.new(Element::Blunt, sp: 0, power: 100, name: "толкает"),
  "сжимает"        => Attack.new(Element::Blunt, sp: 5, power: 200, name: "сжимает"),
  "Кусает (ядом)"  => Attack.new(Element::Poison, sp: 5, power: 250, name: "кусает"),
  "плюется (ядом)" => Attack.new(Element::Poison, sp: 3, power: 200, name: "плюется"),
}

BEHOLDER1_ATTACK = Attack.new(Element::Chaos, sp: 0, power: 100, name: "смотрит")
BEHOLDER2_ATTACK = Attack.new(Element::Mana, sp: 10, power: 300, name: "жжет")
SWARM_ATTACK     = Attack.new(Element::Blunt, sp: 1, power: 150, name: "бросает")
FLEA_ATTACK      = Attack.new(Element::Poison, sp: 0, power: 100, name: "пьет кровь")
FROG_ATTACK      = Attack.new(Element::Blunt, sp: 0, power: 150, name: "прыгает")
SPIDER_ATTACK    = Attack.new(Element::Blunt, sp: 10, power: 300, name: "клеит")
SHADE_ATTACK     = Attack.new(Element::Cold, sp: 0, power: 120, name: "пугает")
SLIME_ATTACK     = Attack.new(Element::Acid, sp: 5, power: 200, name: "поглощает")
HOUND_ATTACK     = Attack.new(Element::Mana, sp: 5, power: 200, name: "дышит")
PHOENIX_ATTACK   = Attack.new(Element::Fire, sp: 10, power: 250, name: "сжигает")
LIZARD_ATTACK    = Attack.new(Element::Chaos, sp: 0, power: 100, name: "пестрит")

UTYP_DATA = {
  UTyp::Beholder => {name: "бехолдер", gender: 1, genitive: "бехолдера", spells: 3, stats: {UPS::MaxSP => 1}, resistances: {Element::Pierce => -1, Element::Fire => 1, Element::Acid => 1, Element::Cold => 1, Element::Electricity => 1, Element::Poison => 2, Element::Light => -1, Element::Dark => -1, Element::Mana => 1, Element::Chaos => 1}, attacks: {BEHOLDER1_ATTACK, BEHOLDER2_ATTACK}},
  UTyp::Swarm    => {name: "вихрь", gender: 1, genitive: "вихря", spells: 3, stats: {UPS::MaxHP => -1, UPS::Crit => 1, UPS::Initiative => 1}, resistances: {Element::Blunt => 1, Element::Slash => 1, Element::Pierce => 1, Element::Mana => -1, Element::Chaos => 1}, attacks: {BASIC_ATTACKS["рвет"], BASIC_ATTACKS["толкает"], SWARM_ATTACK}},
  UTyp::Wolf     => {name: "волк", gender: 1, genitive: "волка", spells: 1, stats: {UPS::MaxHP => 1, UPS::Damage => 1}, resistances: {Element::Pierce => 1, Element::Cold => 1, Element::Dark => 1}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["кусает"]}},
  UTyp::Crow     => {name: "ворон", gender: 1, genitive: "ворона", spells: 2, stats: {UPS::MaxSP => 1, UPS::Crit => 1, UPS::Initiative => -1}, resistances: {Element::Blunt => 1, Element::Electricity => 1}, attacks: {BASIC_ATTACKS["клюет"], BASIC_ATTACKS["рвет"]}},
  UTyp::Aspid    => {name: "гадюка", gender: 0, genitive: "гадюку", spells: 0, stats: {UPS::Crit => 1}, resistances: {Element::Cold => -1, Element::Poison => 1}, attacks: {BASIC_ATTACKS["сжимает"], BASIC_ATTACKS["Кусает (ядом)"]}},
  UTyp::Hydra    => {name: "гидра", gender: 0, genitive: "гидру", spells: 1, stats: {UPS::MaxHP => 1, UPS::Damage => 1, UPS::Initiative => -1}, resistances: {Element::Slash => -1, Element::Pierce => 1, Element::Poison => 1, Element::Mana => -1}, attacks: {BASIC_ATTACKS["кусает"], BASIC_ATTACKS["толкает"], BASIC_ATTACKS["сжимает"], BASIC_ATTACKS["Кусает (ядом)"]}},
  UTyp::Golem    => {name: "голем", gender: 1, genitive: "голема", spells: 2, stats: {UPS::MaxHP => 1, UPS::MaxSP => -1, UPS::Damage => 1, UPS::Crit => -1, UPS::Armor => 1, UPS::Initiative => -1}, resistances: {Element::Blunt => 2, Element::Slash => 1, Element::Pierce => 1, Element::Fire => 1, Element::Cold => 1, Element::Electricity => 1, Element::Poison => 1, Element::Light => 1, Element::Dark => 1, Element::Mana => -1, Element::Chaos => -1}, attacks: {BASIC_ATTACKS["рвет"], BASIC_ATTACKS["толкает"], BASIC_ATTACKS["сжимает"]}},
  UTyp::Grifon   => {name: "грифон", gender: 1, genitive: "грифона", spells: 1, stats: {UPS::MaxHP => 1, UPS::Damage => 1, UPS::Initiative => 1}, resistances: {Element::Electricity => 1}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["кусает"], BASIC_ATTACKS["рвет"], BASIC_ATTACKS["толкает"]}},
  UTyp::Bug      => {name: "жук", gender: 1, genitive: "жука", spells: 1, stats: {UPS::Crit => -1, UPS::Armor => 1}, resistances: {Element::Pierce => -1, Element::Fire => -1, Element::Acid => 1, Element::Poison => 1}, attacks: {BASIC_ATTACKS["кусает"], BASIC_ATTACKS["толкает"], BASIC_ATTACKS["Кусает (ядом)"]}},
  UTyp::Crane    => {name: "журавль", gender: 1, genitive: "журавля", spells: 0, stats: {UPS::MaxSP => 1, UPS::Crit => 1}, resistances: {Element::Blunt => 1}, attacks: {BASIC_ATTACKS["клюет"], BASIC_ATTACKS["толкает"]}},
  UTyp::Rabbit   => {name: "зайчик", gender: 1, genitive: "зайчика", spells: 1, stats: {UPS::Damage => -1, UPS::Initiative => 2}, resistances: {Element::Cold => 1}, attacks: {BASIC_ATTACKS["кусает"], FROG_ATTACK}},
  UTyp::Cobra    => {name: "кобра", gender: 0, genitive: "кобру", spells: 0, stats: {UPS::Damage => 1}, resistances: {Element::Pierce => 1, Element::Cold => -1}, attacks: {BASIC_ATTACKS["кусает"], BASIC_ATTACKS["Кусает (ядом)"], BASIC_ATTACKS["плюется (ядом)"]}},
  UTyp::Flee     => {name: "комар", gender: 1, genitive: "комара", spells: 0, stats: {UPS::MaxHP => -1, UPS::Damage => 1, UPS::Armor => -1, UPS::Initiative => 1}, resistances: {Element::Blunt => -1, Element::Slash => 1}, attacks: {BASIC_ATTACKS["кусает"], FLEA_ATTACK}},
  UTyp::Cat      => {name: "кошка", gender: 0, genitive: "кошку", spells: 2, stats: {UPS::MaxHP => 1, UPS::MaxSP => -1, UPS::Damage => 1, UPS::Crit => 1, UPS::Armor => -1, UPS::Initiative => 1}, resistances: {Element::Dark => 2}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["кусает"], BASIC_ATTACKS["рвет"]}},
  UTyp::Frog     => {name: "лягушка", gender: 0, genitive: "лягушку", spells: 0, stats: {UPS::Damage => 1, UPS::Armor => -1}, resistances: {Element::Acid => 1, Element::Electricity => 1}, attacks: {BASIC_ATTACKS["кусает"], BASIC_ATTACKS["Кусает (ядом)"], FROG_ATTACK}},
  UTyp::Bear     => {name: "медведь", gender: 1, genitive: "медведя", spells: 0, stats: {UPS::MaxHP => 1, UPS::Damage => 1, UPS::Crit => -1, UPS::Armor => 1}, resistances: {Element::Blunt => 1, Element::Slash => 1, Element::Pierce => -1, Element::Cold => 1}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["рвет"], BASIC_ATTACKS["толкает"], BASIC_ATTACKS["сжимает"]}},
  UTyp::Ant      => {name: "муравей", gender: 1, genitive: "муравья", spells: 0, stats: {UPS::MaxHP => -1, UPS::MaxSP => 1, UPS::Damage => 1, UPS::Initiative => -1}, resistances: {Element::Poison => 1}, attacks: {BASIC_ATTACKS["кусает"], BASIC_ATTACKS["толкает"], BASIC_ATTACKS["Кусает (ядом)"]}},
  UTyp::Fly      => {name: "муха", gender: 0, genitive: "муху", spells: 0, stats: {UPS::MaxSP => 1, UPS::Damage => -1, UPS::Armor => -1, UPS::Initiative => 1}, resistances: {Element::Blunt => 1}, attacks: {BASIC_ATTACKS["жалит"], BASIC_ATTACKS["кусает"], BASIC_ATTACKS["Кусает (ядом)"]}},
  UTyp::Bat      => {name: "нетопырь", gender: 1, genitive: "нетопыря", spells: 2, stats: {UPS::MaxSP => 1, UPS::Crit => 1}, resistances: {Element::Light => -1, Element::Dark => 2}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["кусает"]}},
  UTyp::Bee      => {name: "оса", gender: 0, genitive: "осу", spells: 0, stats: {UPS::Damage => 1}, resistances: {Element::Light => 1}, attacks: {BASIC_ATTACKS["жалит"], BASIC_ATTACKS["кусает"]}},
  UTyp::Panther  => {name: "пантера", gender: 0, genitive: "пантеру", spells: 0, stats: {UPS::MaxHP => 1, UPS::MaxSP => -1, UPS::Damage => 1, UPS::Crit => 1, UPS::Armor => -1, UPS::Initiative => 1}, resistances: {Element::Pierce => 1, Element::Light => 1, Element::Dark => 1}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["кусает"], BASIC_ATTACKS["рвет"], BASIC_ATTACKS["толкает"]}},
  UTyp::Spider   => {name: "паук", gender: 1, genitive: "паука", spells: 2, stats: {UPS::MaxHP => 1, UPS::MaxSP => 1, UPS::Initiative => -1}, resistances: {Element::Slash => 1, Element::Chaos => 1}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["жалит"], BASIC_ATTACKS["кусает"], BASIC_ATTACKS["плюется (ядом)"], SPIDER_ATTACK}},
  UTyp::Shade    => {name: "призрак", gender: 1, genitive: "призрака", spells: 3, stats: {UPS::MaxHP => -1, UPS::Armor => 1}, resistances: {Element::Blunt => 2, Element::Slash => 2, Element::Pierce => 2, Element::Fire => 1, Element::Acid => 1, Element::Cold => 1, Element::Poison => 2, Element::Light => -2, Element::Dark => 2}, attacks: {BASIC_ATTACKS["рвет"], BASIC_ATTACKS["толкает"], BASIC_ATTACKS["сжимает"], SHADE_ATTACK}},
  UTyp::Skeleton => {name: "скелет", gender: 1, genitive: "скелета", spells: 3, stats: {UPS::Damage => -1, UPS::Armor => 1, UPS::Initiative => -1}, resistances: {Element::Slash => 1, Element::Pierce => 2, Element::Cold => 1, Element::Poison => 2, Element::Light => -1, Element::Dark => 2}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["кусает"], BASIC_ATTACKS["рвет"], BASIC_ATTACKS["толкает"]}},
  UTyp::Scorpion => {name: "скорпион", gender: 1, genitive: "скорпиона", spells: 0, stats: {UPS::Damage => 1, UPS::Crit => 1, UPS::Armor => 1}, resistances: {Element::Slash => 1, Element::Fire => -1}, attacks: {BASIC_ATTACKS["жалит"], BASIC_ATTACKS["кусает"], BASIC_ATTACKS["плюется (ядом)"]}},
  UTyp::Slime    => {name: "слизневик", gender: 1, genitive: "слизневика", spells: 2, stats: {UPS::MaxHP => 1, UPS::MaxSP => 1, UPS::Damage => -1, UPS::Crit => -1, UPS::Armor => -1, UPS::Initiative => -1}, resistances: {Element::Blunt => 2, Element::Slash => 1, Element::Pierce => 1, Element::Poison => 1}, attacks: {BASIC_ATTACKS["сжимает"], BASIC_ATTACKS["плюется (ядом)"], SLIME_ATTACK}},
  UTyp::Dog      => {name: "собака", gender: 0, genitive: "собаку", spells: 2, stats: {UPS::MaxHP => 1}, resistances: {Element::Fire => 1, Element::Cold => 1, Element::Poison => 1}, attacks: {BASIC_ATTACKS["кусает"], BASIC_ATTACKS["рвет"], BASIC_ATTACKS["толкает"], HOUND_ATTACK}},
  UTyp::Hawk     => {name: "сокол", gender: 1, genitive: "сокола", spells: 1, stats: {UPS::MaxHP => -1, UPS::MaxSP => 1, UPS::Crit => 1}, resistances: {Element::Blunt => 1}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["клюет"], BASIC_ATTACKS["рвет"]}},
  UTyp::Tiger    => {name: "тигр", gender: 1, genitive: "тигра", spells: 0, stats: {UPS::MaxHP => 1, UPS::MaxSP => -1, UPS::Damage => 1, UPS::Crit => 1, UPS::Armor => -1, UPS::Initiative => 1}, resistances: {Element::Slash => 1, Element::Dark => 1}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["кусает"], BASIC_ATTACKS["рвет"], BASIC_ATTACKS["толкает"]}},
  UTyp::Anaconda => {name: "удав", gender: 1, genitive: "удава", spells: 0, stats: {UPS::MaxHP => -1, UPS::Damage => 1, UPS::Armor => 1, UPS::Initiative => -1}, resistances: {Element::Blunt => 1, Element::Poison => 1}, attacks: {BASIC_ATTACKS["кусает"], BASIC_ATTACKS["сжимает"]}},
  UTyp::Snail    => {name: "улитка", gender: 0, genitive: "улитку", spells: 1, stats: {UPS::MaxHP => -1, UPS::MaxSP => 1, UPS::Damage => -1, UPS::Crit => -1, UPS::Armor => 1, UPS::Initiative => -2}, resistances: {Element::Blunt => 1, Element::Slash => 1, Element::Pierce => 1, Element::Fire => 1, Element::Acid => 1, Element::Cold => 1, Element::Electricity => 1, Element::Light => 1, Element::Dark => 1, Element::Mana => 1}, attacks: {BASIC_ATTACKS["толкает"], BASIC_ATTACKS["плюется (ядом)"]}},
  UTyp::Phoenix  => {name: "феникс", gender: 1, genitive: "феникса", spells: 3, stats: {UPS::MaxHP => 1, UPS::MaxSP => 1, UPS::Damage => 1, UPS::Initiative => 1}, resistances: {Element::Fire => 3, Element::Mana => 1, Element::Chaos => 1}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["клюет"], PHOENIX_ATTACK}},
  UTyp::Chimera  => {name: "химера", gender: 0, genitive: "химеру", spells: 3, stats: {UPS::MaxHP => 1, UPS::Damage => 1, UPS::Armor => 1, UPS::Initiative => -1}, resistances: {Element::Blunt => -1, Element::Slash => 1, Element::Pierce => -1, Element::Fire => 1, Element::Acid => -1, Element::Cold => 1, Element::Electricity => -1, Element::Poison => 1, Element::Light => -1, Element::Dark => 1, Element::Mana => -1, Element::Chaos => 1}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["клюет"], BASIC_ATTACKS["жалит"], BASIC_ATTACKS["кусает"], BASIC_ATTACKS["рвет"], BASIC_ATTACKS["толкает"], BASIC_ATTACKS["сжимает"], BASIC_ATTACKS["Кусает (ядом)"], BASIC_ATTACKS["плюется (ядом)"]}},
  UTyp::Worm     => {name: "червь", gender: 1, genitive: "червя", spells: 0, stats: {UPS::MaxHP => 1, UPS::Damage => -1, UPS::Initiative => -1}, resistances: {Element::Blunt => 1, Element::Pierce => 1, Element::Poison => 1, Element::Light => 1, Element::Dark => 1}, attacks: {BASIC_ATTACKS["кусает"], BASIC_ATTACKS["сжимает"], BASIC_ATTACKS["Кусает (ядом)"]}},
  UTyp::Bird     => {name: "ястреб", gender: 1, genitive: "ястреба", spells: 1, stats: {UPS::MaxHP => -1, UPS::Crit => 1, UPS::Initiative => 1}, resistances: {Element::Blunt => 1, Element::Light => 1}, attacks: {BASIC_ATTACKS["когтит"], BASIC_ATTACKS["клюет"], BASIC_ATTACKS["рвет"]}},
  UTyp::Lizard   => {name: "ящерица", gender: 0, genitive: "ящерицу", spells: 1, stats: {UPS::MaxHP => 1, UPS::MaxSP => 1}, resistances: {Element::Slash => 2, Element::Acid => 1, Element::Poison => -1}, attacks: {BASIC_ATTACKS["кусает"], BASIC_ATTACKS["Кусает (ядом)"], BASIC_ATTACKS["плюется (ядом)"], LIZARD_ATTACK}},
}

MIN_LEVEL        = 0
MAX_LEVEL        = 4
N_FOR_LEVEL      = {8, 30, 45, 25, 5}
SPELLS_FREQUENCY = 0.5

def spells_for_unit(level, spells)
  return 0 if spells == 0
  x = 0
  (level + spells - 1).times { x += 1 if rand < SPELLS_FREQUENCY }
  x = 2 if x > 2
  x
end

def grammar_adj(name, gender : Bool, genitive : Bool)
  y = name[-2] == 'ы'
  s = "??"
  if gender
    if genitive
      s = y ? "ого" : "его"
    else
      s = y ? "ый" : "ий"
    end
  else
    if genitive
      s = "ую"
    else
      s = "ая"
    end
  end
  name.gsub("ий", s).gsub("ый", s)
end

def generate_monsters
  log("generating")
  full_list = [] of SomeUnit
  used_names = Set(String).new
  (MIN_LEVEL..MAX_LEVEL).each do |lvl|
    log("lvl #{lvl}")
    candidates = UNITS_DATA.select { |item| item[:level] == lvl }
    candidates.sample(N_FOR_LEVEL[lvl]).each do |item|
      # log("typ")
      typ_data = UTYP_DATA[item[:typ]]
      # log("name")
      name = ""
      genitive = ""
      element = Element::None
      suffix_stat = nil
      level = item[:level]
      if level == 0
        name = typ_data[:name].capitalize
        genitive = typ_data[:genitive]
        element = item[:element]
        element = FREQUENT_ELEMENTS.sample if element == Element::None
      else
        guard = 0
        loop do
          element = item[:element]
          element = (Element.values - [Element::None]).sample if element == Element::None
          prefixes = UNITS_PREFIX.select { |x| x[:element] == element && x[:level] == level }
          prefixes = UNITS_PREFIX.select { |x| x[:element] == element && (x[:level] - level).abs < 2 } if prefixes.size == 0
          prefixes = UNITS_PREFIX.select { |x| x[:element] == element } if prefixes.size == 0
          prefix = prefixes.sample
          name = "#{grammar_adj(prefix[:name], typ_data[:gender] == 1, false)} #{typ_data[:name]}"
          genitive = "#{grammar_adj(prefix[:name], typ_data[:gender] == 1, true)} #{typ_data[:genitive]}"
          if rand < 0.3 && level > 1
            suffix_stat = UPS.values.sample
            name = "#{name}#{SUFFIX_NAMES[suffix_stat][0]}"
            genitive = "#{genitive}#{SUFFIX_NAMES[suffix_stat][1]}"
          else
            suffix_stat = nil
          end
          break unless used_names.includes? name
          guard += 1
          break if guard > 1000
        end
        next if guard > 1000
      end
      used_names << name
      u = SomeUnit.new(name, item[:typ], item[:tile], level, element)
      u.genitive = genitive
      log("monster: #{name}, #{genitive}")
      # now do stats
      # log("stats")
      u.params[UPS::Initiative] = 20 - level*4
      u.params[UPS::MaxHP] = 20 + 10*level
      stat_scores = {level*5, 1}.max
      stat_chances = UPS.values.map { |stat| (typ_data[:stats][stat]? || 0) + 2 }
      stat_scores.times do |i|
        stat = UPS.values.weighted_sample(stat_chances)
        u.params[stat] += STAT_PER_POINT[stat]
      end
      if suffix_stat
        u.params[suffix_stat] += u.params[suffix_stat]//2
      end
      log("maxhp: #{u.params[UPS::MaxHP]}")
      # now do resistances
      # log("resistances")
      Element.values.each do |elem|
        u.resistance[elem] = typ_data[:resistances][elem]? || 0
      end
      elem_scores = 2*level
      add_scores = level > 0 ? rand(level + 1) : rand(2)
      counter = COUNTER_ELEMENT[element]?
      add_scores.times do |i|
        if counter && i == 0 && rand < 0.5
          bad = counter.not_nil!
        else
          bad = (Element.values - [element, Element::None]).sample
        end
        u.resistance[bad] -= 1
      end
      (elem_scores + add_scores).times do |i|
        elem = (Element.values - [Element::None]).sample
        next if rand < 0.5 && FREQUENT_ELEMENTS.includes?(elem) # TODO - more justified cost
        u.resistance[elem] += 1
      end
      # now do attacks
      # log("attacks")
      attack_scores = level
      possible_list = typ_data[:attacks].to_a
      # log("a1")
      if possible_list.size < 2
        raise("too little attacks for #{typ_data}")
      elsif possible_list.size < 3
        u.attacks = possible_list.dup
      else
        u.attacks = [possible_list.sample, possible_list.sample]
      end
      good_list = possible_list - u.attacks
      u.attacks[1] = u.attacks[1].replace_element(element)
      guard = 0
      # log("a2")
      while guard < 1000
        guard += 1
        case rand(3)
        # new attack
        when 0
          # log("a3:0")
          if good_list.size > 0
            att = good_list.sample
            u.attacks << att
            good_list.delete att
            attack_scores -= 1
          end
          # log("a3:+")
          # change element
        when 1
          # log("a3:1")
          i = rand(u.attacks.size) # TODO - more optimal
          att = u.attacks[i]
          if att.element != element
            newelem = rand < 0.5 ? element : (Element.values - [Element::None, att.element]).sample
            u.attacks[i] = att.replace_element(newelem)
            attack_scores -= 1
          end
          # log("a3:1+")
        when 2
          # log("a3:2")
          i = rand(u.attacks.size) # TODO - more optimal
          att = u.attacks[i]
          if newatt = att.improve(u.params[UPS::MaxSP])
            # log("a3:2.1")
            u.attacks[i] = newatt
            attack_scores -= 1
          elsif newatt = att.make_freer
            # log("a3:2.2")
            u.attacks[i] = newatt
            attack_scores -= 1
          end
          # log("a3:2+")
        end
        break if attack_scores <= 0
      end
      # now do spells
      spells_for_unit(level, typ_data[:spells]).times do
        sp1 = Spell.generate(u)
        u.spells << sp1 unless u.spells.any? { |x| x.typ == sp1.typ }
      end
      raise "incorrect attack generation: #{u.params[UPS::MaxSP]}: #{u.attacks}" if guard >= 1000
      # simple attack should exist
      # log("simple attack")
      # pp! u.attacks, good_list, possible_list
      if !u.attacks.any? { |att| att.sp == 0 }
        list = good_list.select { |att| att.sp == 0 }
        list = possible_list.select { |att| att.sp == 0 } if list.size == 0
        # log("me")
        if list.size == 0
          att = possible_list.sample.make_free
        else
          att = list.sample
        end
        u.attacks << att
      end
      # log("simple attack done")
      # no attack should be higher maxhp
      u.attacks.map! { |x| x.depower(u.params[UPS::MaxHP], u.params[UPS::Damage]) }

      u.attacks.sort_by! { |x| -x.sp }

      full_list << u
    end
  end
  log("generating done")
  GC.collect
  full_list
end
