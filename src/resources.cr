include Engine

THE_SCREEN = Sprite.new(-1)
NO_MUSIC   = Sound.new(-1)

module RES
  module Btn
    Btn         = ButtonResource.new(0)
    Empty       = ButtonResource.new(1)
    Speedactive = ButtonResource.new(2)
    Speed       = ButtonResource.new(3)
  end

  module Tiles
    Affected   = TileMap.new(0)
    Cast       = TileMap.new(1)
    Characters = TileMap.new(2)
    Ground     = TileMap.new(3)
    Portraits  = TileMap.new(4)
    Present    = TileMap.new(5)
    Seal       = TileMap.new(6)
    Units      = TileMap.new(7)
  end

  Arial     = FontResource.new(0)
  Battle    = Sound.new(0)
  Consola   = FontResource.new(1)
  Field     = Sprite.new(0)
  Hourglass = Sprite.new(1)
  Music     = Sound.new(1)
  Stair     = Sprite.new(2)
end
