TILE_SIZE   = 64
PLAYER_SIZE = 48

DEMO_SIZE = 100

ORIG_TILE_SIZE   = 32
ORIG_PLAYER_SIZE = 24

TILE_SCALE   = TILE_SIZE / ORIG_TILE_SIZE
PLAYER_SCALE = PLAYER_SIZE / ORIG_PLAYER_SIZE

PLAYER_SPEED = 10
ENEMY_SPEED  =  7

WALK_ANIM_SPEED = 100
WALK_ANIM_STOP  =  50

PLAYER_FACE = 15
PLAYER_TILE =  6
ENEMY_FACES = {8, 9, 10, 11, 12}
ENEMY_TILES = {2, 3, 4, 5, 6}

FLOOR_TILES = {1, 2}
WALL_TILES  = {0, 4, 6, 10}

RENDER_RADIUS_X = 12
RENDER_RADIUS_Y =  8

DIFFICULTY_RATE      = 0.9
MONSTERS_COUNT       = 100
LOOT_COUNT           =  10
STAIRS_COUNT         =   4
CAPTURE_CHANCE_START =  25
CAPTURE_CHANCE_MAX   =  80

HEAL_POWER   =  0.3
SPELLS_USAGE = 0.33

enum UseFont
  Small
  Usual
  Log
  SmallGreen
  SmallRed
end

FONTS = {} of UseFont => Font

PLAYER_NAME = "Девочка"

module Enumerable(T)
  # TODO - iterator support?
  def weighted_sample(weights : (Array | Tuple)) : T
    total = weights.sum
    point = rand * total
    zip(weights) do |n, w|
      return n if w >= point
      point -= w
    end
    return first # to prevent "nillable" complain
  end
end

UNIT_GRADES = {Color::WHITE, 0x00ff00ff, 0x005fffff, 0xff5f00ff, Color::RED}

def grade_color(level)
  n = {level, UNIT_GRADES.size - 1}.min
  UNIT_GRADES[n]
end

def show_monster_card(tile, level)
  RES::Tiles::Units.draw_frame(tile, *GUI.center)
  rect(*GUI.pos, *GUI.size, false, grade_color(level))
end
