require "./unitsgen.cr"

module Enumerable(T)
  # TODO - iterator support?
  def weighted_sample(weights : (Array | Tuple)) : T
    total = weights.sum
    point = rand * total
    zip(weights) do |n, w|
      return n if w >= point
      point -= w
    end
    return first # to prevent "nillable" complain
  end
end

def log(s)
  puts s
end

100.times do
  generate_monsters
end
