require "./spells"

enum UPS
  MaxHP; MaxSP; Damage; Armor; Crit; Initiative
end

PARAM_NAMES = {
  UPS::MaxHP      => "ХП",
  UPS::MaxSP      => "CП",
  UPS::Damage     => "Сила",
  UPS::Armor      => "Броня",
  UPS::Crit       => "Крит",
  UPS::Initiative => "Скорость",
}

PARAM_GENITIVE = {
  UPS::MaxHP      => "ХП",
  UPS::MaxSP      => "CП",
  UPS::Damage     => "Силу",
  UPS::Armor      => "Броню",
  UPS::Crit       => "Крит",
  UPS::Initiative => "Скорость",
}

enum Element
  Blunt; Slash; Pierce; Fire; Acid; Cold; Electricity; Poison; Light; Dark; Mana; Chaos; None
end

DAMAGE_NAMES = {
  Element::Blunt       => "дробящего",
  Element::Slash       => "режущего",
  Element::Pierce      => "колющего",
  Element::Fire        => "огненного",
  Element::Acid        => "кислотного",
  Element::Cold        => "ледяного",
  Element::Electricity => "электро",
  Element::Poison      => "ядовитого",
  Element::Light       => "светлого",
  Element::Dark        => "темного",
  Element::Mana        => "магического",
  Element::Chaos       => "хаотичного",
}

RESIST_NAMES = {
  Element::Blunt       => "дробящее",
  Element::Slash       => "режущее",
  Element::Pierce      => "колющее",
  Element::Fire        => "огонь",
  Element::Acid        => "кислота",
  Element::Cold        => "лёд",
  Element::Electricity => "молнии",
  Element::Poison      => "яд",
  Element::Light       => "свет",
  Element::Dark        => "тьма",
  Element::Mana        => "мана",
  Element::Chaos       => "хаос",
}

record Attack, element : Element, sp : Int32, power : Int32, name : String do
  def replace_element(to_elem)
    Attack.new(to_elem, sp, power, name)
  end

  def make_free
    Attack.new(element, 0, power, name)
  end

  def make_freer : Attack?
    return nil if sp < 3
    Attack.new(element, sp - 1, power, name)
  end

  def improve(maxsp) : Attack?
    return nil if power > 400 || sp >= maxsp - 5
    Attack.new(@element, sp + 5, power*2, name)
  end

  def depower(maxhp, damage)
    if power * damage // 100 >= maxhp
      p_new = (maxhp - 1) * 100 // damage
      return Attack.new(element, sp, p_new, name)
    else
      return self
    end
  end
end

BATTLE_LOG   = [] of String
BATTLE_WIDTH = 80

def battle_log(s)
  line = ""
  # puts s
  s.split(' ').each do |tok|
    if tok.size + line.size < BATTLE_WIDTH
      line += " " + tok
    else
      BATTLE_LOG << line
      line = tok
    end
  end
  BATTLE_LOG << line if line != ""
end

class SomeUnit
  property params = Hash(UPS, Int32).new(1)
  property resistance = Hash(Element, Int32).new(0)
  property name : String
  property typ : UTyp
  property genitive : String
  property hp = 1
  property level : Int32
  property element : Element
  property sp = 1
  property attacks = [] of Attack
  property spells = [] of Spell

  getter tile : Int32
  property drawn_x = 0.0f32
  property drawn_y = 0.0f32
  property parent : SomeUnit?

  property delay = 0
  property param_improved = Hash(UPS, Int32).new(0)
  property resist_improved = Hash(Element, Int32).new(0)

  def initialize(@name, @typ, @tile, @level, @element)
    @parent = nil
    @genitive = @name
    @params[UPS::MaxHP] = 20
    @params[UPS::MaxSP] = 10
    @params[UPS::Damage] = 4
    @params[UPS::Crit] = 10
    @params[UPS::Armor] = 0
    @params[UPS::Initiative] = 10
    @resistance[element] = 1
    @attacks = [] of Attack
    @spells = [] of Spell
  end

  def clone
    it = SomeUnit.new(@name, @typ, @tile, @level, @element)
    it.params = @params.clone
    it.genitive = @genitive
    it.resistance = @resistance.clone
    it.hp = @params[UPS::MaxHP]
    it.sp = @params[UPS::MaxSP]
    it.attacks = @attacks.clone
    it.spells = @spells.clone
    it.parent = self
    it
  end

  def take_hit(damage, element)
    if @resistance[element] < 0
      (-@resistance[element]).times { damage = damage + damage // 2 }
    else
      @resistance[element].times { damage = damage // 2 }
    end
    if rand < 0.5 && @params[UPS::Armor] > 1
      damage -= rand(@params[UPS::Armor])
    else
      damage -= @params[UPS::Armor]
    end
    damage = 1 if damage < 1
    @hp -= damage
    log_die if dead
  end

  def attack(target, safely = false)
    att = attacks.select { |a| a.sp <= @sp }.sample
    damage = att.power * params[UPS::Damage] // 100
    damage = 1 if damage < 1
    if target.resistance[att.element] < 0
      (-target.resistance[att.element]).times { damage = damage + damage // 2 }
    else
      target.resistance[att.element].times { damage = damage // 2 }
    end
    if rand < 0.5 && target.params[UPS::Armor] > 1
      damage -= rand(target.params[UPS::Armor])
    else
      damage -= target.params[UPS::Armor]
    end
    damage = 1 if damage < 1
    return false if damage >= target.hp && safely
    was_crit = rand(100) < params[UPS::Crit]
    damage = damage*2 if was_crit
    crit_str = was_crit ? " (КРИТИЧЕСКИЙ УДАР x2)" : ""
    resist_str = case target.resistance[att.element]
                 when .<(0)
                   ", #{target.name} уязвим к нему"
                 when .>(1)
                   ", #{target.name} почти невосприимчив к нему"
                 when .>(0)
                   ", #{target.name} сопротивляется ему"
                 else
                   ""
                 end
    battle_log "#{@name} #{att.name} #{target.genitive}#{crit_str}, нанося #{damage} #{DAMAGE_NAMES[att.element]} урона #{resist_str}"
    @sp -= att.sp
    target.hp -= damage
    target.log_die if target.dead
    true
  end

  def log_die
    battle_log "#{@name} теряет сознание"
  end

  def dead
    @hp <= 0
  end

  def capture_chance
    hp_rate = @hp * 100 // @params[UPS::MaxHP]
    return 0 if hp_rate > CAPTURE_CHANCE_START
    return (CAPTURE_CHANCE_START - hp_rate) * CAPTURE_CHANCE_MAX // CAPTURE_CHANCE_START
  end
end
