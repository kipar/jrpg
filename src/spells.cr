require "./battle"

enum Magic
  Heal
  Stun
  Splash
  DrainSP
  Buff
  Debuff
  BuffResist
  DebuffResist
end

MAGIC_COST   = [5, 10, 10, 5, 5, 5, 5, 5]
MAGIC_CHANCE = [10, 10, 10, 10, 5, 5, 5, 5]

record Spell, typ : Magic, sp : Int32, param : UPS, element : Element, power : Int32 do
  def name
    case typ
    when .heal?
      "лечит союзников"
    when .stun?
      "шокирует"
    when .splash?
      "массовая атака #{power} #{DAMAGE_NAMES[element]} урона"
    when .drain_sp?
      "вытягивает #{power} сп"
    when .buff?
      "усиливает #{PARAM_GENITIVE[param]} союзника"
    when .debuff?
      "ослабляет #{PARAM_GENITIVE[param]} противника"
    when .buff_resist?
      "усиливает защиту от #{DAMAGE_NAMES[element]} урона"
    when .debuff_resist?
      "ослабляет защиту от #{DAMAGE_NAMES[element]} урона"
    end
  end

  def apply(who, we, enemies)
    case typ
    when .heal?
      damaged = we.select { |x| x.hp < (1 - HEAL_POWER)*x.params[UPS::MaxHP] }
      damaged = we.select { |x| x.hp < x.params[UPS::MaxHP] } if damaged.size == 0
      return nil if damaged.size == 0
      unit = damaged.sample
      maxhp = unit.params[UPS::MaxHP]
      healhp = (maxhp*HEAL_POWER).to_i
      healhp = 1 if healhp < 1
      healhp = {healhp, maxhp - unit.hp}.min
      unit.hp += healhp
      battle_log "#{who.name} лечит #{unit.genitive} на #{healhp}"
      return [unit]
    when .stun?
      unit = enemies.sample
      d = 1
      d -= 1 if rand < 0.1
      d += 1 if rand < 0.1
      unit.delay += d
      battle_log "#{who.name} шокирует #{unit.genitive} на #{d} ходов"
      return [unit]
    when .drain_sp?
      unit = enemies.sample
      delta = {unit.sp, power}.min
      unit.sp -= delta
      who.sp += delta
      who.sp = {who.params[UPS::MaxSP], who.sp}.min
      battle_log "#{who.name} лишает #{unit.genitive} #{delta} сп"
      return [unit]
    when .splash?
      battle_log "#{who.name} атакует ВСЕХ врагов на #{power} #{DAMAGE_NAMES[element]} урона"
      enemies.each &.take_hit(power, element)
      return enemies
    when .buff?
      candidates = we.select { |x| x.param_improved[param] <= 0 }
      return nil if candidates.size == 0
      x = candidates.sample
      x.param_improved[param] += 1
      x.params[param] = (x.params[param] * 1.5).to_i
      battle_log "#{who.name} усиливает #{x.genitive}: #{PARAM_NAMES[param]} теперь #{x.params[param]}"
      return [x]
    when .debuff?
      candidates = enemies.select { |x| x.param_improved[param] >= 0 }
      return nil if candidates.size == 0
      x = candidates.sample
      x.param_improved[param] -= 1
      x.params[param] = (x.params[param] / 1.5).to_i
      battle_log "#{who.name} ослабляет #{x.genitive}: #{PARAM_NAMES[param]} теперь #{x.params[param]}"
      return [x]
    when .buff_resist?
      candidates = we.select { |x| x.resist_improved[element] <= 0 }
      return nil if candidates.size == 0
      x = candidates.sample
      x.resist_improved[element] += 1
      x.resistance[element] += 1
      battle_log "#{who.name} повышает защиту от #{DAMAGE_NAMES[element]} урона (цель - #{x.name}) до #{x.resistance[element]}"
      return [x]
    when .debuff_resist?
      candidates = enemies.select { |x| x.resist_improved[element] >= 0 }
      return nil if candidates.size == 0
      x = candidates.sample
      x.resist_improved[element] -= 1
      x.resistance[element] -= 1
      battle_log "#{who.name} понижает защиту от #{DAMAGE_NAMES[element]} урона (цель - #{x.name})} до #{x.resistance[element]}"
      return [x]
    end
  end

  def self.generate(u)
    typ = Magic.values.weighted_sample(MAGIC_CHANCE)
    sp = {MAGIC_COST[typ.to_i], u.params[UPS::MaxSP]}.min
    param = UPS::MaxHP
    element = Element::None
    power = 0
    case typ
    when .splash?
      power = u.params[UPS::Damage]
      element = (Element.values - [Element::None]).sample
    when .drain_sp?
      power = u.params[UPS::Damage]
    when .buff?
      param = (UPS.values - [UPS::MaxHP, UPS::MaxSP]).sample
    when .debuff?
      param = (UPS.values - [UPS::MaxHP, UPS::MaxSP]).sample
    when .buff_resist?
      element = (Element.values - [Element::None]).sample
    when .debuff_resist?
      element = (Element.values - [Element::None]).sample
    end
    Spell.new(typ, sp, param, element, power)
  end
end
