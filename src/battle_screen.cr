require "./libnonoengine.cr"
require "./scales"
require "./battle"
require "./map"
require "./spells"

enum AnimationType
  Attack; Cast; Die; Hit; Capture; Affected
end

ANIM_TIME = {
  AnimationType::Attack   => 5,
  AnimationType::Cast     => 5,
  AnimationType::Die      => 8,
  AnimationType::Hit      => 2,
  AnimationType::Capture  => 12,
  AnimationType::Affected => 5,
}

class Animation # TODO - struct then move counter out
  getter who
  getter what

  @counter : Int32 = 0

  def initialize(@speed : Int32, @who : SomeUnit, @what : AnimationType, @target2 : SomeUnit? = nil)
  end

  def draw
    stage = @counter / (ANIM_TIME[what]*@speed)
    case what
    when .attack?
      t = @target2
      raise "animation error" unless t
      if stage < 0.5
        # moving to target
        shift = stage*2
      else
        # moving back
        shift = (1 - stage)*2
      end
      px = @who.drawn_x + (t.drawn_x - @who.drawn_x)*shift
      py = @who.drawn_y + (t.drawn_y - @who.drawn_y)*shift
      RES::Tiles::Units.draw_frame(@who.tile, px, py)
    when .die?
      RES::Tiles::Units.draw_frame(@who.tile, @who.drawn_x, @who.drawn_y, color: Color::RED, kx: 1 - stage)
    when .hit?
      RES::Tiles::Units.draw_frame(@who.tile, @who.drawn_x, @who.drawn_y, color: Color::RED, kx: 1 - stage*2)
    when .capture?
      RES::Tiles::Units.draw_frame(@who.tile, @who.drawn_x, @who.drawn_y, kx: 1 - stage)
      RES::Tiles::Seal.draw_frame((stage * 6).to_i, @who.drawn_x, @who.drawn_y)
    when .cast?
      RES::Tiles::Units.draw_frame(@who.tile, @who.drawn_x, @who.drawn_y)
      RES::Tiles::Cast.draw_frame((stage * 5).to_i, @who.drawn_x, @who.drawn_y)
    when .affected?
      RES::Tiles::Units.draw_frame(@who.tile, @who.drawn_x, @who.drawn_y)
      RES::Tiles::Affected.draw_frame((stage * 5).to_i, @who.drawn_x, @who.drawn_y) if stage > 0
    end
  end

  def process(battle)
    @counter += 1
    return @counter > ANIM_TIME[what]*@speed
  end
end

abstract class Action
  abstract def name : String
  abstract def desc : Array(String)

  def initialize(@battle : Battle)
  end

  def possible
    true
  end
end

abstract class SelectAction < Action
  def can_select(side : Int32, unit : SomeUnit) : Bool
    false
  end

  abstract def do_action(side : Int32, unit : SomeUnit)
end

abstract class NoSelectAction < Action
  abstract def do_action
end

class Battle
  property forces : {Array(SomeUnit), Array(SomeUnit)}
  property reserve : {Array(SomeUnit), Array(SomeUnit)}
  property faces : {Int32, Int32}
  getter game : Game
  getter order : Array({Int32, SomeUnit})
  @cur_index = 0
  @cur_timer = 0
  getter animations
  getter hp_for_reserve = {} of SomeUnit => Int32

  @actions : Array(Action)
  @sel_action : Action?
  property dont_kill : SomeUnit?
  property fleeing : Bool
  @u_tooltip : SomeUnit? = nil
  @u_enemy = false
  @a_tooltip : Action? = nil
  @@anim_speed : Int32 = 2
  property replacing : SomeUnit?

  def update_order
    @order = @forces[0].map { |x| {0, x} } + @forces[1].map { |x| {1, x} }
    @order.shuffle!
    @order.sort_by! { |x, u| -u.params[UPS::Initiative] }
  end

  def initialize(@game, @forces, @reserve, @faces)
    @order = [] of {Int32, SomeUnit}
    update_order

    @u_tooltip = nil
    @a_tooltip = nil
    BATTLE_LOG.clear
    battle_log("Начинается бой")
    @animations = [] of Animation
    @turning = false
    @selecting = false
    @sel_action = nil
    # TODO - incremental text render
    # @shown_count = 0
    @fleeing = false
    @dont_kill = nil
    @actions = [] of Action
    init_actions
  end

  def inv_selected(u : SomeUnit)
    old = @replacing.not_nil!
    oldbase = old.parent.not_nil!
    i = @forces[0].index(old)
    raise "Error returning unit" unless i
    # remove old unit into inventory
    @game.player.inventory << oldbase
    @hp_for_reserve[oldbase] = old.hp
    # insert new unit
    newcomer = u.clone
    @forces[0][i] = newcomer
    newcomer.hp = @hp_for_reserve[u] if @hp_for_reserve[u]?
    # delete it from inventory
    @game.player.inventory.delete(u)
    @hp_for_reserve.delete(u)
    update_order
    battle_log "#{u.name} выходит на поле боя"
    start_turn
  end

  def init_actions
    @actions.clear
    {Wait, HealUnit, CareUnit, ReplaceUnit, Capture, DontKill, CanKill, Flee}.each { |x| @actions << x.new(self) }
  end

  MAX_LOG = 12

  def show_log
    panel(30, 20, 650, 300, HAlign::Right, VAlign::Bottom, fill: color(0x001000ff)) do
      n = {BATTLE_LOG.size, MAX_LOG}.min
      n.times do |i|
        str = BATTLE_LOG[i + (BATTLE_LOG.size - n)]
        label(str, FONTS[UseFont::Log], 10, 5, 650, 20, HAlign::Left, VAlign::Flow)
      end
    end
  end

  def start_selection(action)
    @sel_action = action
    @selecting = true
  end

  def start_turn
    @sel_action = nil
    @selecting = false
    @cur_index = 0
    # @animations.clear
    @turning = true
    GC.collect
  end

  def end_turn
    @turning = false
    @order.reject! { |s, x| x.dead }
    @forces.each_with_index do |force, side|
      force.reject! do |x|
        @game.list_lost << x if side == 0 && x.dead
        x.dead
      end
      if force.size < MAX_BATTLE && @reserve[side].size > 0
        MAX_BATTLE.times do
          alive = @reserve[side].dup
          if side == 0
            @hp_for_reserve.each do |u, hp|
              alive.delete(u) if hp <= 0
            end
            break if alive.size == 0
          end
          max_level = alive.max_of(&.level)
          x = alive.select! { |u| u.level == max_level }.sample
          returning = x.clone
          force << returning
          returning.hp = @hp_for_reserve[x] if side == 0 && @hp_for_reserve[x]?
          @reserve[side].delete(x)
          @hp_for_reserve.delete(x) if side == 0
          break if @reserve[side].size == 0
          break if force.size == MAX_BATTLE
        end
        update_order
      end
      if force.size == 0
        @game.end_battle(true)
        return
      end
    end
    if @fleeing
      @game.end_battle(false)
    end
  end

  def process_turns
    if @animations.size == 0
      do_turn
    else
      @animations.shift if @animations[0].process(self)
    end
  end

  def do_turn
    if @cur_index >= @order.size
      end_turn
      return
    end
    side, who = @order[@cur_index]
    @cur_index += 1
    return if @fleeing && side == 0
    return if who.dead
    if who.delay > 0
      who.delay -= 1
      return
    end
    if rand < SPELLS_USAGE && who.spells.size > 0
      list = who.spells.select { |s| s.sp <= who.sp }
      if list.size > 0
        spell = list.sample
        who.sp -= spell.sp
        f1 = @forces[side].select { |x| !x.dead }
        f2 = @forces[1 - side].select { |x| !x.dead }
        return if f1.size == 0 || f2.size == 0
        if targets = spell.apply(who, f1, f2)
          @animations << Animation.new(@@anim_speed, who, AnimationType::Cast)
          targets.each do |aff|
            @animations << Animation.new(@@anim_speed, aff, AnimationType::Affected)
          end
          return
        end
      end
    end
    x = @forces[1 - side].select { |u| u.hp > 0 }
    return if x.size == 0
    target = x.sample
    if @dont_kill == target
      failed = !who.attack(target, safely: true)
      if failed
        x.delete(target)
        return if x.size == 0
        target = x.sample
        who.attack(target)
      end
    else
      who.attack(target)
    end
    @animations << Animation.new(@@anim_speed, who, AnimationType::Attack, target)
    if target.hp < 0
      @animations << Animation.new(@@anim_speed, target, AnimationType::Die)
    else
      @animations << Animation.new(@@anim_speed, target, AnimationType::Hit)
    end
  end

  def show_board
    @u_tooltip = nil
    2.times do |side|
      panel(0, 0, 80, 400, side == 0 ? HAlign::Left : HAlign::Right, VAlign::Center) do
        @forces[side].each do |u|
          panel(10, 10, 64, 64, HAlign::Left, VAlign::Flow) do
            xc, yc = GUI.center
            xg, yg = GUI.cursor
            u.drawn_x = xc
            u.drawn_y = yc
            if @animations.any? { |x| x.who == u }
              @animations.each { |x| x.draw if x.who == u }
              next
            end
            next if u.dead

            show_monster_card(u.tile, u.level)
            RES::Hourglass.draw(*GUI.center) if u.delay > 0
            if @dont_kill == u
              line_settings(width: 5)
              rect(*GUI.pos, *GUI.size, false, 0xff8800ff)
              line_settings(width: 1)
            end
            if @selecting && @sel_action.not_nil!.as(SelectAction).can_select(side, u)
              line_settings(width: 5)
              rect(*GUI.pos, *GUI.size, false, 0x00ff00ff)
              line_settings(width: 1)
              if Mouse.left.clicked? && (0..1).includes?(xg) && (0..1).includes?(yg)
                @sel_action.not_nil!.as(SelectAction).do_action(side, u)
                start_turn unless @game.inventory.visible
                return
              end
            end
            if (0..1).includes?(xg) && (0..1).includes?(yg)
              @u_tooltip = u
              @u_enemy = side == 1
            end
            if u.hp < u.params[UPS::MaxHP]
              panel(0, 15, 54, 10, HAlign::Center, VAlign::Bottom) do
                rect_gauge(*GUI.pos, *GUI.size, u.hp / u.params[UPS::MaxHP], color(0x00ff00ff), color(0xff0000ff))
              end
            end
            if u.sp < u.params[UPS::MaxSP]
              panel(0, 5, 54, 10, HAlign::Center, VAlign::Bottom) do
                rect_gauge(*GUI.pos, *GUI.size, u.sp / u.params[UPS::MaxSP], color(0x0000ffff), color(0x000000ff))
              end
            end
          end
        end
      end
    end
  end

  def speed_btn(x, y, spd)
    v = 8 // spd
    button(@@anim_speed == v ? RES::Btn::Speedactive : RES::Btn::Speed, x, y, 32, 32, "x" + spd.to_s, HAlign::Center, VAlign::Top, font: FONTS[UseFont::Usual]) do |st|
      if st.pressed?
        p v
        @@anim_speed = v
      end
    end
  end

  def draw
    @a_tooltip = nil
    Engine.layer = LAYER_GUI
    # portraits
    panel(20, 20, 6*48, 8*48, HAlign::Left) do
      RES::Tiles::Portraits.draw_frame(@faces[0], *GUI.center, kx: 6, ky: 8)
    end
    panel(20, 20, 6*48, 8*48, HAlign::Right) do
      RES::Tiles::Portraits.draw_frame(@faces[1], *GUI.center, kx: 6, ky: 8)
    end
    panel(30, 20, 300, 300, valign: VAlign::Bottom) do
      if !@turning
        @actions.each_with_index do |act, i|
          next unless act.possible
          button(RES::Btn::Btn, 0, 0, 300, 30, act.name, HAlign::Center, VAlign::Flow, font: FONTS[UseFont::Usual]) do |x|
            if x.clicked? && !@turning
              if act.is_a? SelectAction
                start_selection(act)
              elsif act.is_a? NoSelectAction
                act.do_action
                start_turn
              end
            elsif x.hover?
              @a_tooltip = act
            end
          end
        end
      end
    end

    show_log
    panel(0, 40, 400, 400, HAlign::Center, VAlign::Top) do
      x1, y1 = GUI.pos
      w, h = GUI.size
      RES::Field.tex_triangle(x1, y1, x1 + h, y1, x1 + h, y1 + h, kx: 0.5, ky: 0.5)
      RES::Field.tex_triangle(x1, y1, x1 + h, y1 + h, x1, y1 + h, kx: 0.5, ky: 0.5)
      rect(*GUI.pos, *GUI.size, false, 0x00ffffff)
      show_board
      if @selecting
        label("Выберите цель", FONTS[UseFont::Usual], 0, -20, 400, 30, HAlign::Center, VAlign::Top, fill: color(0x400000ff), text_halign: HAlign::Center)
      else
        speed_btn(-70, -20, 1)
        speed_btn(-30, -20, 2)
        speed_btn(10, -20, 4)
        speed_btn(50, -20, 8)
      end
    end
    show_tooltip
  end

  def process
    RES::Battle.music
    process_turns if @turning
  end

  def get_anim_speed
    @@anim_speed
  end

  def multiline(font, x0, y0, dy, list)
    ypos = 0
    list.each do |str|
      font.draw_text(str, x0, y0 + ypos)
      ypos += dy
    end
  end

  def show_tooltip
    if u = @u_tooltip
      elems = Element.values.select { |elem| u.resistance[elem] != 0 }
      resistances = elems.map do |elem|
        "#{RESIST_NAMES[elem]}: #{u.resistance[elem]}"
      end
      panel(Mouse.x + 30, Mouse.y + 30, 400, 125 + resistances.size*12, fill: color(0x888888ff)) do
        x0, y0 = GUI.pos
        FONTS[UseFont::Small].draw_text(u.name, x0 + 5, y0)
        params = UPS.values.map do |param|
          case param
          when .max_hp?
            str = "#{u.hp}/#{u.params[param]}"
          when .max_sp?
            str = "#{u.sp}/#{u.params[param]}"
          else
            str = "#{u.params[param]}"
          end
          "#{PARAM_NAMES[param]}: #{str}"
        end
        attacks = u.attacks.map { |att|
          "#{att.sp > 0 ? "(#{att.sp}сп)" : ""}#{att.name}: #{att.power*u.params[UPS::Damage]//100} #{DAMAGE_NAMES[att.element]} урона"
        } + u.spells.map { |sp|
          "(#{sp.sp}сп)#{sp.name}"
        }
        if @sel_action.is_a?(Capture) && @u_enemy
          chance = u.capture_chance
          (FONTS[chance == 0 ? UseFont::SmallRed : UseFont::SmallGreen]).draw_text("Шанс захвата: #{chance}%", x0 + 115, y0 + 0)
        end
        multiline(FONTS[UseFont::Small], x0 + 10, y0 + 15, 12, params)
        multiline(FONTS[UseFont::Small], x0 + 110, y0 + 15, 12, attacks)
        FONTS[UseFont::Small].draw_text("Сопротивления:", x0 + 5, y0 + 100)
        multiline(FONTS[UseFont::Small], x0 + 10, y0 + 115, 12, resistances)
      end
    end

    Engine.layer = LAYER_GUI + 1
    if a = @a_tooltip
      panel(Mouse.x + 30, Mouse.y + 30, 650, height: 125, fill: color(0x888888ff)) do
        x0, y0 = GUI.pos
        multiline(FONTS[UseFont::Small], x0 + 10, y0 + 10, 12, a.desc)
      end
    end
  end
end

class HealUnit < SelectAction
  def name : String
    "Лечить питомца"
  end

  def desc : Array(String)
    ["Восстанавливает хп зверушки на 30% от макс. значения"]
  end

  def can_select(side : Int32, unit : SomeUnit) : Bool
    side == 0 && unit.hp < unit.params[UPS::MaxHP]
  end

  def do_action(side : Int32, unit : SomeUnit)
    maxhp = unit.params[UPS::MaxHP]
    healhp = (maxhp*HEAL_POWER).to_i
    healhp = 1 if healhp < 1
    healhp = {healhp, maxhp - unit.hp}.min
    unit.hp += healhp
    battle_log "#{PLAYER_NAME} лечит #{unit.genitive} на #{healhp}"
  end
end

class CareUnit < SelectAction
  def name : String
    "Ободрить питомца"
  end

  def desc : Array(String)
    ["Восстанавливает сп зверушки на 50% от макс. значения"]
  end

  def can_select(side : Int32, unit : SomeUnit) : Bool
    side == 0 && unit.sp < unit.params[UPS::MaxSP]
  end

  def do_action(side : Int32, unit : SomeUnit)
    maxsp = unit.params[UPS::MaxSP]
    old = unit.sp
    unit.sp = {unit.sp + maxsp//2, maxsp}.min
    battle_log "#{PLAYER_NAME} ободряет #{unit.genitive} на #{unit.sp - old}сп"
  end
end

class ReplaceUnit < SelectAction
  def name : String
    "Сменить зверушку"
  end

  def desc : Array(String)
    ["Заменяет одну из дерущихся зверушек на зверушку из резерва"]
  end

  def can_select(side : Int32, unit : SomeUnit) : Bool
    side == 0
  end

  def do_action(side : Int32, unit : SomeUnit)
    @battle.replacing = unit
    @battle.game.inventory.show_for(@battle)
  end
end

class Capture < SelectAction
  def name : String
    "Захватить противника"
  end

  def desc : Array(String)
    ["Попытаться захватить раненую зверушку противника", "Шанс зависит от хп цели"]
  end

  def can_select(side : Int32, unit : SomeUnit) : Bool
    side == 1 && unit.capture_chance > 0
  end

  def do_action(side : Int32, unit : SomeUnit)
    if rand(100) < unit.capture_chance
      battle_log "#{unit.name} захвачен в плен"
      @battle.animations << Animation.new(@battle.get_anim_speed, unit, AnimationType::Capture)
      item = unit.parent.not_nil!
      @battle.game.player.inventory << item
      @battle.game.list_captured << item
      @battle.hp_for_reserve[item] = -1
      unit.hp = -1
      @battle.update_order
    else
      battle_log "Захват не удался!"
    end
  end
end

class DontKill < SelectAction
  def name : String
    "Приказать брать живым"
  end

  def desc : Array(String)
    ["Приказывает постараться не наносить добивающие удары по цели", "Критический удар все еще может убить цель"]
  end

  def can_select(side : Int32, unit : SomeUnit) : Bool
    side == 1
  end

  def do_action(side : Int32, unit : SomeUnit)
    @battle.dont_kill = unit
  end
end

class CanKill < NoSelectAction
  def name : String
    "Разрешить добивать"
  end

  def desc : Array(String)
    ["Отменяет приказ 'брать живым'"]
  end

  def possible
    return false unless u = @battle.dont_kill
    !u.dead
  end

  def do_action
    @battle.dont_kill = nil
  end
end

class Flee < NoSelectAction
  def name : String
    "Убегать"
  end

  def desc : Array(String)
    ["Убежать с поля боя"]
  end

  def do_action
    @battle.fleeing = true
  end
end

class Wait < NoSelectAction
  def name : String
    "Ждать"
  end

  def desc : Array(String)
    ["Ничего не делать (зверушки будут действовать сами)"]
  end

  def do_action
    battle_log "#{PLAYER_NAME} выжидает"
  end
end
