require "./libnonoengine.cr"
require "./scales"

def f_xy(x, y, width)
  x + y*width
end

MAGIC_DXY = {
  {1, 0}, # 1
  {2, 0}, # 2
  {1, 1}, # 3
  {2, 1}, # 4
  {0, 2}, # 5
  {1, 2}, # 6
  {2, 2}, # 7
  {0, 3}, # 8
  {1, 3}, # 9
  {2, 3}, # 10
  {0, 4}, # 11
  {1, 4}, # 12
  {2, 4}, # 13
  {0, 5}, # 14
  {1, 5}, # 15
  {2, 5}, # 16
}

VAR_BASE = 9
VAR_LIST = {9, 14, 15, 16}

PROBLEM_CORNERS = {1, 2, 3, 4}

CORNER_PATCH =
  {
    {1, 10, 12, 1},
    {8, 2, 2, 12},
    {6, 3, 3, 10},
    {4, 6, 8, 4},

  }

REVERT_Y = {
  3,  # 1
  4,  # 2
  1,  # 3
  2,  # 4
  11, # 5
  12, # 6
  13, # 7
  8,  # 8
  9,  # 9
  10, # 10
  5,  # 11
  6,  # 12
  7,  # 13
}

REVERT_X = {
  2,  # 1
  1,  # 2
  4,  # 3
  3,  # 4
  7,  # 5
  6,  # 6
  5,  # 7
  10, # 8
  9,  # 9
  8,  # 10
  13, # 11
  12, # 12
  11, # 13
}

NEIGHMAP = {
  5, 5, 8, 2, 6, 3, 4, 9,
}

# neighs bits:
# 1 - dx dy
# 2 - 0 dy
# 4 - dx 0
def calculate_corner(index, corner_dx, corner_dy, neighs, rng)
  nindex = (neighs[0] ? 1 : 0) + (neighs[1] ? 2 : 0) + (neighs[2] ? 4 : 0)
  c = NEIGHMAP[nindex]
  c = REVERT_X[c - 1] if corner_dx > 0
  c = REVERT_Y[c - 1] if corner_dy > 0
  if PROBLEM_CORNERS.includes? c
    # if cor
    c = CORNER_PATCH[c - 1][index]
  end
  c = VAR_LIST.sample(rng) if c == VAR_BASE && rng.rand < 0.1
  return c
end

CORNERS = {
  {-1, -1},
  {1, -1},
  {-1, 1},
  {1, 1},
}

def show_ground(size_x, size_y, index, resource, res_width, px, py, &)
  basex = index*3
  basey = 0
  size_y.times do |coord_y|
    next if (coord_y - py).abs > RENDER_RADIUS_Y
    size_x.times do |coord_x|
      next if (coord_x - px).abs > RENDER_RADIUS_X
      next unless yield(coord_x, coord_y)
      seed = Random.new(coord_x*1000 + coord_y)

      CORNERS.each_with_index do |(corner_dx, corner_dy), index|
        other_x, other_y = coord_x + corner_dx, coord_y + corner_dy
        other_x_ok = (0...size_x).includes? other_x
        other_y_ok = (0...size_y).includes? other_y
        neighs = {
          (other_x_ok && other_y_ok) ? yield(other_x, other_y) : true,
          other_y_ok ? yield(coord_x, other_y) : true,
          other_x_ok ? yield(other_x, coord_y) : true,
        }
        num = calculate_corner(index, corner_dx, corner_dy, neighs, seed)
        offset = MAGIC_DXY[num - 1]
        resource.draw_frame(f_xy(basex + offset[0], basey + offset[1], res_width), coord_x*TILE_SIZE + corner_dx*TILE_SIZE//4, coord_y*TILE_SIZE + corner_dy*TILE_SIZE//4, kx: TILE_SCALE/2, ky: TILE_SCALE/2)
        # resource.draw_frame(f_xy(basex + offset[0], basey + offset[1], res_width), coord_x*32*2 + corner_dx*16, coord_y*32*2 + corner_dy*16, kx: 1, ky: 1)
      end
    end
  end
end
